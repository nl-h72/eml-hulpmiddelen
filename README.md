# EML Hulpmiddelen

Na elke verkiezing in Nederland worden de verkiezingsuitslagen door de Kiesraad gepubliceerd in het EML formaat.
EML (Election Markup Language) is een internationale standard om verkiezingsresultaten te publiceren.
Voor Nederland is de Kiesraad verantwoordelijk voor de Nederlandse variant van deze standard.
Zie [https://www.kiesraad.nl/verkiezingen/osv-en-eml/eml-standaard](https://www.kiesraad.nl/verkiezingen/osv-en-eml/eml-standaard)

De bibliotheek in dit Java project is een hulpmiddel om EML bestanden in te lezen.
De ingelezen bestanden worden omgezet naar specifieke EML versie Java record objecten,
die weer gebruikt kunnen worden om verder te verwerken.

## Gebruik

De Java bibliotheek werkt met Java versie 17 of hoger.

### Maven

Om deze Java bibliotheek te gebruiken moet het jar bestand worden toegevoegd aan je project.
De bibliotheek is beschikbaar via GitHub packages.
Als maven wordt gebruikt voeg dan de volgende repository toe aan het pom.xml bestand van je project:

```
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/53628543/packages/maven</url>
  </repository>
</repositories>
```

Om de bibliotheek te gebruiken voeg de volgende dependency toe aan je pom.xml:

```
<dependency>
  <groupId>nl.h72.eml</groupId>
  <artifactId>eml-hulpmiddelen</artifactId>
  <version>1.0.0-SNAPSHOT</version>
</dependency>
```

### Java Gebruik

#### EmlLezer

Voor het werken met de EML bestanden is er een class `EmlLezer` waarmee eenvoudig een enkel of meerdere bestanden kunnen worden ingelezen om verwerkt te worden.
Deze class heeft een methode `verwerk` die als argument een implementatie van de interface `EmlVerwerker` verwacht,
en een directory naar EML bestanden of specifiek EML bestand.
Een instantie van een `EmlLezer` object kan gemaakt worden met de `EmlLezerBouwer`.
Hiermee kunnen aanvullende opties worden toegevoegd aan de `EmlLezer`.
Een aantal voorbeelden van hoe het gebruikt kan worden.

```java
EmlVerwerker verwerker = ... // Initialiseer met eigen implementatie van EmlVerwerker interface.

// Maak een EmlLezer met de gegeven verwerker.
EmlLezer lezer = EmlLezer.bouwer(verwerker).bouw();

// Verwerk recursief de bestanden in de opgeven directory.
lezer.verwerk(verwerker, Paths.of("/mijn/directory/met/eml/bestanden"));

// Verwerk recursief de bestanden in de opgeven directory en waarbij de 'filter' Predicate true geeft.
// Dit kan gebruikt worden om alleen specifieke bestanden in te lezen.
lezer.verwerk(verwerker, Paths.of("/mijn/directory/met/eml/bestanden", filter));

// Verwerk het opgegeven bestand.
lezer.verwerk(verwerker, new File("/mijn/directory/met/eml/bestanden/een_510b_bestand.eml.xml"));
```

#### ParallelEmlVerwerker

Om optimaler gebruikt te maken van meerdere core systemen is er een aanvulling om bestanden parallel te kunnen verwerken.
Hiervoor is de class `ParallelVerwerk` beschikbaar.
Met deze class kan 

```java
  // Het volgende stuk code toont hoe deze ParallelEmlVerwerker kan worden gebruikt
  // om eml510 bestanden gelijktijdig te verwerken.
  ...
  EmlVerwerker verwerker = ... // Initialiseer met eigen implementatie van EmlVerwerker interface.

  // Maak een nieuwe instantie aan met de verwerker, die 3 processen gelijktijdig kan afhandelen.
  final ParallelEmlVerwerker parallelEmlLezer = new ParallelEmlVerwerker(verwerker, 3);
  // Start the verwerker met de specifieke taak die moet worden uitgevoerd.
  parallelEmlLezer.verwerk(bouwer -> verwerkEml510(bouwer, directory));
  ...

 // Deze methode wordt in het in parallel verwerker uitgevoerd met een bouwer die de emllezer initialiseert
 // om gelijktijdig meerdere bestanden te kunnen verwerken.
 // Deze methode moet de eml lezer aanmaken en vervolgens verwerk aanroepen om het proces te starten.
private static void verwerkEml510(final EmlLezerBouwer bouwer, final Path directory) {
  final EmlLezer emlLezer = bouwer.logger(System.out::println).bouw();

  EmlBestandHulpmiddel.VOORVOEGSEL_510_BESTANDEN.forEach(voorvoegsel ->  emlLezer.verwerk(directory, f -> filterBestanden(f, voorvoegsel)));
}
 ```

#### EmlVerwerker

De interface `EmlVerwerker` heeft voor elk EML type een methode.
Bij het inlezen van een EML bestand wordt dit omgezet naar een voor het EML bestand specifiek object.
Afhankelijk van het type object wordt dan in the `EmlLezer` de methode voor dat specifieke object aangeroepen.
Implementaties van de `EmlVerwerker` interface kunnen dan het ontvangen EML object verder verwerken.
Ook wordt de naam van het originele bestand waar de EML gegevens zijn uitgelezen meegegeven.
De interface kan 4 type EML formaten lezen: EML 110a, EML230, EML510(b,c,d) en EML520.
Een implementatie van de interface hoeft niet alle methoden te implementeren, maar alleen die voor welke objecten gewenst zijn om te verwerken.
Een voorbeeld vana een implementatie ziet er als volgt uit:

```java
public class MijnVerwerker implements EmlVerwerker {
  public void verwerk(File bestandnaam, Eml230 eml230) {
    // Verwerk eml gegevens
  }

  public void verwerk(File bestandnaam, Eml510 eml510) {
    // Verwerk eml gegevens
  }
```

## OSV2020, XSD en EML

EML, ofwel XML bestanden bevatten gestructureerde gegevens.
Onderdeel hiervan is dat in XSD bestanden de beschrijving van XML kan worden vastgelegd.
Dankzij deze gestructureerde manier van beschrijven is het ook weer mogelijk om broncode te genereren die deze XML bestanden kan lezen.
Binnen Java is hiervan de JAXB bibliotheek de bekendste open source bibliotheek waarmee het mogelijk is om broncode te genereren die XML bestanden kan inlezen.
Van de EML, en specifiek de Nederlandse uitbreidingen, stelt de Kiesraad ook de XSD bestanden beschikbaar.
In theorie is het daarmee mogelijk om Java broncode te genereren op basis van deze XSD bestanden.
Helaas blijkt het in praktijk wat lastiger, omdat de XSD beschrijving van EML niet helemaal goed aansluit bij wat JAXB nodig heeft.
Aangezien de Kiesraad de broncode van OSV2020 heeft vrijgegeven is het mogelijk om te kijken hoe de Kiesraad dit zelf heeft opgelost.
Uit de broncode blijkt dat er een specifieke XSD is gemaakt, genaamd `110a-210-230-510-520-extended.xsd` die gebruikt is om broncode te genereren.
En ook blijkt er een XJB bestand, `110a-210-230-510-520-binding.xjb`, te zijn gebruikt om de uitzonderingsgevallen goed door JAXB te laten afhandelen.
Helaas zijn deze bestanden niet meegeleverd met de broncode van OSV2020 en is het dus lastig om zelf de code generatie uit te voeren.
Echter het resultaat van deze generatie van broncode is wel meegeleverd met de broncode van OSV2020.
Daarom is dat deel van de broncode van OSV2020 in dit project toegevoegd om daarop voort te bouwen.
Omdat de licentie van deze broncode niet heel duidelijk is (bij de broncode staat alleen dat het niet verder verspreid mag worden) valt deze broncode niet onder de licentie van dit project.
Het omvat de broncode in the namespace `de.ivu`, `oasis` en `org.w3`.

## Licentie

Copyright 2023-2024 EML-Hulpmiddelen bijdragers

In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden goedgekeurd door de
Europese  Commissie - latere versies van de EUPL (De "Licentie");

U mag dit werk alleen gebruiken in overeenstemming met de licentie.
U kunt een kopie van de licentie verkrijgen op:

https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen, wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op een "AS IS"-basis,
ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK, expliciet of impliciet.
Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen onder de Licentie van toepassing zijn.

Deze licentie is niet van toepassing op de broncode in de namespaces beginnende met `de.ivu`, `oasis` en `org.w3`.
Op die broncode is de copyright van de Kiesraad van toepassing.
