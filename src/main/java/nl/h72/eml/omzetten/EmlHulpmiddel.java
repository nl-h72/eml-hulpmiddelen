/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import de.ivu.wahl.wus.oasis.eml.ElectionIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.kiesraad.ElectionDomain;
import de.ivu.wahl.wus.oasis.eml.kiesraad.ObjectFactory;

/**
 * Helpmiddel class om specifieke gegevens in een EML bestand uit te splitsen.
 */
final class EmlHulpmiddel {

  /**
   * Gegevens van een stemburo.
   */
  record Stembureau(String naam, String postcode) {
  }

  /**
   * Woord 'Stembureau' als match om te verwijderen als dit voor de naam van het stembureau staat.
   */
  private static final String STEMBUREAU = "Stembureau";

  /**
   * Reguliere expressie patroon om stembureau naam en postcode op te splitsen als de postode in de naam zit.
   */
  private static final String POSTCODE_PREIFX = "\\(postcode: ";

  private EmlHulpmiddel() {
    // Eml Hulpmiddel class
  }

  /**
   * Geef het {@link ElectionDomain} terug uit {@link ElectionIdentifierStructure}. Dit zit niet in de 510 en 520 wrappers terwijl het object er wel
   * in zit.
   *
   * @param electionIdentifier
   * @return {@link ElectionDomain}
   */
  public static ElectionDomain findeElectionDomain(final ElectionIdentifierStructure electionIdentifier) {
    for (final Object o : electionIdentifier.getAny()) {
      if (o instanceof final ElectionDomain ed) {
        return ed;
      }
    }

    final ElectionDomain result = new ObjectFactory().createElectionDomain();
    electionIdentifier.getAny().add(result);
    return result;
  }

  /**
   * Splits de naam van het stembureau en postcode in een niew record. De naam moet voldoen aan het patroon:
   * <code>Stembureau .* (postcode: .*)</code>. Als de naam niet opgesplitst kon worden wordt de originele naam als naam in het record gezet.
   *
   * @param naam om op te splitsen
   * @return record met opgesplitste stembureau/postcode
   */
  public static Stembureau splitStembureauNaam(final String naam) {
    final String veiligeNaam = naam == null ? "" : naam.replace(STEMBUREAU, "").strip();
    final String[] split = veiligeNaam.split(POSTCODE_PREIFX);

    if (split.length == 2) {
      return new Stembureau(split[0].strip(), split[1].substring(0, split[1].length() - 1).replace(" ", ""));
    } else {
      return new Stembureau(veiligeNaam, "");
    }
  }
}
