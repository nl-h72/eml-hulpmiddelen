/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.util.ArrayList;
import java.util.List;

import nl.h72.eml.records.Filter;
import nl.h72.eml.records.GekozenKandidaat;
import nl.h72.eml.records.GekozenPartij;
import nl.h72.eml.records.Kandidaat;
import nl.h72.eml.records.KandidaatStemmen;
import nl.h72.eml.records.Kieskring;
import nl.h72.eml.records.Partij;
import nl.h72.eml.records.PartijStemmen;
import nl.h72.eml.records.RapportageGebied;
import nl.h72.eml.records.Regio;
import nl.h72.eml.records.StemStatistieken;
import nl.h72.eml.records.Verkiezing;

/**
 * Filter om meerdere filters te gebruiken. Alle filters moeten true geven om een true te geven voor in dit filter.
 */
public class SamengesteldEmlFilter implements EmlFilter {

  private final List<EmlFilter> filters = new ArrayList<>();

  public SamengesteldEmlFilter add(final EmlFilter filter) {
    filters.add(filter);
    return this;
  }

  @Override
  public boolean filter(final GekozenKandidaat gekozenKandidaat) {
    return allMatch(gekozenKandidaat);
  }

  @Override
  public boolean filter(final GekozenPartij gekozenPartij) {
    return allMatch(gekozenPartij);
  }

  @Override
  public boolean filter(final Kandidaat kandidaat) {
    return allMatch(kandidaat);
  }

  @Override
  public boolean filter(final KandidaatStemmen kandidaatStemmen) {
    return allMatch(kandidaatStemmen);
  }

  @Override
  public boolean filter(final Kieskring kieskring) {
    return allMatch(kieskring);
  }

  @Override
  public boolean filter(final Partij partij) {
    return allMatch(partij);
  }

  @Override
  public boolean filter(final PartijStemmen partijStemmen) {
    return allMatch(partijStemmen);
  }

  @Override
  public boolean filter(final RapportageGebied rapportageGebied) {
    return allMatch(rapportageGebied);
  }

  @Override
  public boolean filter(final Regio regio) {
    return allMatch(regio);
  }

  @Override
  public boolean filter(final StemStatistieken stemStatistieken) {
    return allMatch(stemStatistieken);
  }

  @Override
  public boolean filter(final Verkiezing verkiezing) {
    return allMatch(verkiezing);
  }

  private boolean allMatch(final Filter filter) {
    return filters.stream().allMatch(filter::filter);
  }
}
