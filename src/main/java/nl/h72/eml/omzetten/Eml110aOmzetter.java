/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.util.List;
import java.util.Optional;

import de.ivu.elect.nl.eml.readable.Eml110aWrapper;
import de.ivu.wahl.wus.oasis.eml.EML;
import de.ivu.wahl.wus.oasis.eml.ElectionEvent.Election;
import de.ivu.wahl.wus.oasis.eml.ElectionIdentifierStructure110A;
import de.ivu.wahl.wus.oasis.eml.kiesraad.Committee;
import de.ivu.wahl.wus.oasis.eml.kiesraad.CommitteeCategoryType;
import de.ivu.wahl.wus.oasis.eml.kiesraad.Region;
import de.ivu.wahl.wus.oasis.eml.kiesraad.RegisteredParty;

import nl.h72.eml.records.Eml110a;
import nl.h72.eml.records.Regio;
import nl.h72.eml.records.Verkiezing;

/**
 * Leest de informatie uit een {@link EML} object en zet het om naar een {@link Eml110a} object.
 */
public final class Eml110aOmzetter {

  private static final Short NO_OUDER_REGIO_NUMMER = Short.valueOf((short) -1);

  private final EmlFilter filter;

  public Eml110aOmzetter() {
    this(new EmlFilter() {
    });
  }

  public Eml110aOmzetter(final EmlFilter filter) {
    this.filter = filter;
  }

  /**
   * Leest de informatie uit een {@link EML} object en zet het om naar een {@link Eml110a} object.
   *
   * @param eml {@link EML} object
   * @return {@link Eml110a} object
   */
  public Eml110a zetOm(final EML eml) {
    final Eml110aWrapper wrapper = new Eml110aWrapper(eml);
    final ElectionIdentifierStructure110A eis = wrapper.getElectionIdentifier110a();
    final Election election = wrapper.getElection();
    return new Eml110a(eml.getId(), election.getContest().getContestIdentifier().getId(), verkiezing(eis), election.getNumberOfSeats().intValue(),
        election.getPreferenceThreshold().intValue(), regios(wrapper), geregistreerdePartijen(wrapper));
  }

  private static Verkiezing verkiezing(final ElectionIdentifierStructure110A eis) {
    return Hulpmiddel.verkiezing(eis.getId(), eis.getElectionName(), eis.getElectionCategory().name(), eis.getElectionDate().toString(),
        eis.getElectionDomain());
  }

  private List<Regio> regios(final Eml110aWrapper wrapper) {
    return wrapper.getRegions().stream().map(Eml110aOmzetter::naarRegio).filter(filter::filter).toList();
  }

  private static Regio naarRegio(final Region region) {
    return new Regio(Optional.ofNullable(region.getRegionNumber()).map(Short::intValue).orElse(0), region.getRegionName(), region.getRegionCategory(),
        region.getCommittee().stream().findFirst().map(Committee::getCommitteeCategory).orElse(CommitteeCategoryType.CSB),
        Optional.ofNullable(region.getSuperiorRegionNumber()).orElse(NO_OUDER_REGIO_NUMMER).intValue(),
        Optional.ofNullable(region.getSuperiorRegionCategory()).orElse(null), region.isFrysianExportAllowed(), region.isRomanNumerals(),
        region.getCommittee().stream().map(c -> c.getCommitteeCategory().name()).toList());
  }

  private static List<String> geregistreerdePartijen(final Eml110aWrapper wrapper) {
    return wrapper.getRegisteredParties().stream().map(RegisteredParty::getRegisteredAppellation).toList();
  }
}
