/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.JAXBElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ivu.elect.nl.eml.readable.Eml520ContestWrapper;
import de.ivu.elect.nl.eml.readable.Eml520SelectionWrapper;
import de.ivu.elect.nl.eml.readable.Eml520Wrapper;
import de.ivu.wahl.wus.oasis.eml.AffiliationIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.CandidateStructure;
import de.ivu.wahl.wus.oasis.eml.EML;
import de.ivu.wahl.wus.oasis.eml.ElectionIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.YesNoType;
import de.ivu.wahl.wus.oasis.eml.external.xnl.PersonName;

import nl.h72.eml.records.Eml520;
import nl.h72.eml.records.GekozenKandidaat;
import nl.h72.eml.records.GekozenPartij;

/**
 * Leest de informatie uit een {@link EML} object en zet het om naar een {@link Eml520} object.
 */
public final class Eml520Omzetter {

  private static final Logger LOGGER = LoggerFactory.getLogger(Eml520Omzetter.class);

  private final EmlFilter filter;

  public Eml520Omzetter() {
    this(new EmlFilter() {
    });
  }

  public Eml520Omzetter(final EmlFilter filter) {
    this.filter = filter;
  }

  /**
   * Leest de informatie uit een {@link EML} object en zet het om naar een {@link Eml520} object.
   *
   * @param eml {@link EML} object
   * @return {@link Eml520} object
   */
  public Eml520 zetOm(final EML eml) {
    final Eml520Wrapper wrapper = new Eml520Wrapper(eml);
    final ElectionIdentifierStructure eis = wrapper.getElectionIdentifierStructure();

    return new Eml520(eml.getId(), Hulpmiddel.autoriteit(wrapper.getManagingAuthority().getContent()),
        Hulpmiddel.verkiezing(eis, wrapper.getElectionDate(), EmlHulpmiddel.findeElectionDomain(eis)), kieskringen(wrapper.getContests()));
  }

  private List<GekozenPartij> kieskringen(final List<Eml520ContestWrapper> contests) {
    return contests.stream().findFirst().map(contest -> partijenStemmen(contest.getContestId(), contest.getSelections())).orElse(List.of());
  }

  private List<GekozenPartij> partijenStemmen(final String kieskringId, final List<Eml520SelectionWrapper> selections) {
    final List<GekozenPartij> partijenStemmen = new ArrayList<>();

    List<GekozenKandidaat> kandidatenStemmen = new ArrayList<>();
    AffiliationIdentifierStructure affiliation = null;
    CandidateStructure candidate = null;
    int ranking = -1;
    boolean gekozen = false;

    for (final Eml520SelectionWrapper sel : selections) {
      for (final JAXBElement<?> element : sel.getContent().getContent()) {
        final Object value = element.getValue();
        switch (element.getName().getLocalPart()) {
          case "AffiliationIdentifier":
            if (value instanceof final AffiliationIdentifierStructure ais) {
              if (affiliation != null) {
                voegKandidaatToe(kandidatenStemmen, candidate, ranking, gekozen);
                candidate = null;
                ranking = -1;
                gekozen = false;
                partijenStemmen
                    .add(new GekozenPartij(kieskringId, Integer.parseInt(affiliation.getId()), affiliation.getRegisteredName(), kandidatenStemmen));
                kandidatenStemmen = new ArrayList<>();
              }
              affiliation = ais;
            }
            break;
          case "Candidate":
            if (value instanceof final CandidateStructure cs) {
              voegKandidaatToe(kandidatenStemmen, candidate, ranking, gekozen);
              candidate = cs;
              ranking = -1;
              gekozen = false;
            }
            break;
          case "Ranking":
            ranking = ((BigInteger) value).intValue();
            break;
          case "Elected":
            gekozen = ((YesNoType) value) == YesNoType.YES;
            break;
          default:
            LOGGER.trace("Onbekend element '{}'", element.getName().getLocalPart());
            break;
        }
      }
    }
    if (affiliation != null) {
      voegKandidaatToe(kandidatenStemmen, candidate, ranking, gekozen);
      filter.voegToe(partijenStemmen,
          new GekozenPartij(kieskringId, Integer.parseInt(affiliation.getId()), affiliation.getRegisteredName(), kandidatenStemmen));
    }
    return partijenStemmen;
  }

  private void voegKandidaatToe(final List<GekozenKandidaat> kandidatenStemmen, final CandidateStructure candidate, final int ranking,
      final boolean gekozen) {
    if (candidate != null) {
      filter.voegToe(kandidatenStemmen, maakGekozenKandidaat(candidate, gekozen, ranking));
    }
  }

  private static GekozenKandidaat maakGekozenKandidaat(final CandidateStructure candidate, final boolean gekozen, final int ranking) {
    final PersonName personName = candidate.getCandidateFullName().getPersonName();
    // @formatter:off
    return new GekozenKandidaat(
        Integer.parseInt(candidate.getCandidateIdentifier().getId()),
        candidate.getCandidateIdentifier().getShortCode(),
        Hulpmiddel.geslacht(candidate.getGender()),
        personName.getNameLine().get(0).getContent(),
        personName.getFirstName().get(0).getContent(),
        Optional.ofNullable(personName.getNamePrefix().getContent()).orElse(""),
        personName.getLastName().get(0).getContent(),
        Optional.ofNullable(candidate.getQualifyingAddress().getLocality()).map(l -> l.getLocalityName().get(0).getContent()).orElse(""),
        Optional.ofNullable(candidate.getQualifyingAddress().getCountry()).map(c -> c.getCountryNameCode().get(0).getContent()).orElse(""),
        gekozen,
        ranking);
    // @formatter:on
  }
}
