/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.ivu.elect.nl.eml.readable.Eml510ContestWrapper;
import de.ivu.elect.nl.eml.readable.Eml510Wrapper;
import de.ivu.elect.nl.eml.readable.GroupVotesSection;
import de.ivu.elect.nl.eml.readable.ReportingUnitVotesSectionWrapper;
import de.ivu.elect.nl.eml.readable.ReportingUnitVotesWrapper;
import de.ivu.elect.nl.eml.readable.TotalVotesSectionWrapper;
import de.ivu.elect.nl.eml.readable.TotalVotesWrapper;
import de.ivu.elect.nl.eml.readable.VoteType;
import de.ivu.wahl.wus.oasis.eml.EML;

import nl.h72.eml.omzetten.EmlHulpmiddel.Stembureau;
import nl.h72.eml.records.Autoriteit;
import nl.h72.eml.records.Eml510;
import nl.h72.eml.records.KandidaatStemmen;
import nl.h72.eml.records.Kieskring;
import nl.h72.eml.records.PartijStemmen;
import nl.h72.eml.records.RapportageGebied;
import nl.h72.eml.records.StemStatistieken;
import nl.h72.eml.records.Verkiezing;

/**
 * Leest de informatie uit een {@link EML} object en zet het om naar een {@link Eml510} object.
 */
public final class Eml510Omzetter {

  private final EmlFilter filter;

  public Eml510Omzetter() {
    this(new EmlFilter() {
    });
  }

  public Eml510Omzetter(final EmlFilter filter) {
    this.filter = filter;
  }

  /**
   * Leest de informatie uit een {@link EML} object en zet het om naar een {@link Eml510} object.
   *
   * @param eml {@link EML} object
   * @return {@link Eml510} object
   */
  public Eml510 zetOm(final EML eml) {
    final Eml510Wrapper wrapper = new Eml510Wrapper(eml);
    final Autoriteit autoriteit = Hulpmiddel.autoriteit(eml.getManagingAuthority());
    final Verkiezing verkiezing = Hulpmiddel.verkiezing(wrapper.getElectionIdentifierStructure(), wrapper.getElectionDate(),
        EmlHulpmiddel.findeElectionDomain(wrapper.getElectionIdentifierStructure()));
    final List<Eml510ContestWrapper> contest = wrapper.getContest();
    final String kieskringId = contest.isEmpty() ? "" : contest.get(0).getContestId();

    return new Eml510(eml.getId(), autoriteit, kieskringId, verkiezing, rapportageGebiedTotaal(contest, autoriteit), rapportageGebieden(contest));
  }

  /**
   * Lees het {@link RapportageGebied} van de totaal aantallen.
   *
   * @param contests contests
   * @param autoriteit autoriteit
   * @return {@link RapportageGebied} van totallen
   */
  private RapportageGebied rapportageGebiedTotaal(final List<Eml510ContestWrapper> contests, final Autoriteit autoriteit) {
    return contests.stream().findAny().map(contest -> contest.getTotalVotes().stream().findAny()
        .map(totalVotes -> rapportageGebiedStemmenTotaal(autoriteit, contest, totalVotes)).orElse(null)).orElse(null);
  }

  private RapportageGebied rapportageGebiedStemmenTotaal(final Autoriteit autoriteit, final Eml510ContestWrapper contest,
      final TotalVotesWrapper totalVotes) {
    final TotalVotesSectionWrapper votesWrapper = totalVotes.getVotesWrapper();

    return rapportageGebiedStemmen(autoriteit.id(), autoriteit.naam(), new Kieskring(contest.getContestId(), contest.getContestName(), List.of()),
        votesWrapper.getCandidateVotesPerGroup(), votesWrapper.getValidVotes());
  }

  private List<RapportageGebied> rapportageGebieden(final List<Eml510ContestWrapper> contests) {
    final List<RapportageGebied> rapportageGebieden = new ArrayList<>();

    for (final Eml510ContestWrapper contest : contests) {
      for (final ReportingUnitVotesWrapper reportingUnitVotes : contest.getReportingUnitVotes()) {
        final ReportingUnitVotesSectionWrapper votesWrapper = reportingUnitVotes.getVotesWrapper();
        final RapportageGebied rapportageGebied = rapportageGebiedStemmen(reportingUnitVotes.getId(), reportingUnitVotes.getName(),
            new Kieskring(contest.getContestId(), contest.getContestName(), List.of()), votesWrapper.getCandidateVotesPerGroup(),
            votesWrapper.getValidVotes());

        filter.voegToe(rapportageGebieden, rapportageGebied);
      }
    }
    return rapportageGebieden;
  }

  private RapportageGebied rapportageGebiedStemmen(final String rapportageGebiedId, final String rapportageGebiedNaam, final Kieskring kieskring,
      final Map<GroupVotesSection, Map<Map.Entry<String, String>, BigInteger>> candidateVotesPerGroup, final Map<VoteType, BigInteger> validVotes) {
    final List<PartijStemmen> partijStemmen = candidateVotesPerGroup.entrySet().stream().map(this::partijStemmen).filter(filter::filter).toList();
    final Stembureau stembureau = EmlHulpmiddel.splitStembureauNaam(rapportageGebiedNaam);

    return new RapportageGebied(kieskring, rapportageGebiedId, stembureau.naam(), stembureau.postcode(), partijStemmen,
        new StemStatistieken(validVotes));
  }

  private PartijStemmen partijStemmen(final Entry<GroupVotesSection, Map<Map.Entry<String, String>, BigInteger>> groepStemmen) {
    final List<Entry<Map.Entry<String, String>, BigInteger>> kandidaatStemmen = new ArrayList<>(groepStemmen.getValue().entrySet());
    final List<KandidaatStemmen> kandidatenStemmen = kandidaatStemmen.stream().map(Eml510Omzetter::kandidaatStemmen).filter(filter::filter).toList();
    final GroupVotesSection groep = groepStemmen.getKey();

    return new PartijStemmen(Integer.parseInt(groep.getId()), groep.getRegisteredName(), groep.getVotes().intValue(), kandidatenStemmen);
  }

  /**
   * Maakt kandidaat record aan. De key van {@link KandidaatStemmen} kan of een nummer zijn of de kandidaat code (ShortCode). Indien het een nummer is
   * wordt het als id gebruikt en wordt de kandidaat code op null gezet. Indien het een kandidaat code is, wordt id op 0 gezet, en wordt de kandidaat
   * code gezet in het record.
   *
   * @param kandidaatStemmen id/code + aantal stemmen
   * @return {@link KandidaatStemmen} record
   */
  private static KandidaatStemmen kandidaatStemmen(final Map.Entry<Map.Entry<String, String>, BigInteger> kandidaatStemmen) {
    final Entry<String, String> idCode = kandidaatStemmen.getKey();
    final int id = idCode.getKey() == null || idCode.getKey().isBlank() ? 0 : Integer.parseInt(idCode.getKey());
    final String code = idCode.getValue();

    return new KandidaatStemmen(id, code, kandidaatStemmen.getValue().intValue());
  }
}
