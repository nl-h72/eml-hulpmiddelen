/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.util.List;

import nl.h72.eml.records.Filter;
import nl.h72.eml.records.GekozenKandidaat;
import nl.h72.eml.records.GekozenPartij;
import nl.h72.eml.records.Kandidaat;
import nl.h72.eml.records.KandidaatStemmen;
import nl.h72.eml.records.Kieskring;
import nl.h72.eml.records.Partij;
import nl.h72.eml.records.PartijStemmen;
import nl.h72.eml.records.RapportageGebied;
import nl.h72.eml.records.Regio;
import nl.h72.eml.records.StemStatistieken;
import nl.h72.eml.records.Verkiezing;

/**
 * Interface om specifieke gegevens uit EML bestanden tijdens het inlezen te negeren.
 */
public interface EmlFilter {

  /**
   * Filter {@link GekozenKandidaat}. Als de methode false terug geeft dan is het het idee dat de geven gekozenKandidaat niet terug wordt gegeven.
   *
   * @param gekozenKandidaat om te controleren
   * @return Als true terug geeft dan wordt de gekozenKandidaat meegenomen door de eml lezer.
   */
  default boolean filter(final GekozenKandidaat gekozenKandidaat) {
    return true;
  }

  /**
   * Filter {@link GekozenPartij}. Als de methode false terug geeft dan is het het idee dat de geven gekozenPartij niet terug wordt gegeven.
   *
   * @param gekozenPartij om te controleren
   * @return Als true terug geeft dan wordt de GekozenPartij meegenomen door de eml lezer.
   */
  default boolean filter(final GekozenPartij gekozenPartij) {
    return true;
  }

  /**
   * Filter {@link KandidaatStemmen}. Als de methode false terug geeft dan is het het idee dat de geven kandidaatStemmen niet terug wordt gegeven.
   *
   * @param kandidaatStemmen om te controleren
   * @return Als true terug geeft dan wordt de kandidaatStemmen meegenomen door de eml lezer.
   */
  default boolean filter(final Kandidaat kandidaat) {
    return true;
  }

  /**
   * Filter {@link KandidaatStemmen}. Als de methode false terug geeft dan is het het idee dat de geven kandidaatStemmen niet terug wordt gegeven.
   *
   * @param kandidaatStemmen om te controleren
   * @return Als true terug geeft dan wordt de kandidaatStemmen meegenomen door de eml lezer.
   */
  default boolean filter(final KandidaatStemmen kandidaatStemmen) {
    return true;
  }

  /**
   * Filter {@link Kieskring}. Als de methode false terug geeft dan is het het idee dat de geven kieskring niet terug wordt gegeven.
   *
   * @param Kieskring om te controleren
   * @return Als true terug geeft dan wordt de Kieskring meegenomen door de eml lezer.
   */
  default boolean filter(final Kieskring kieskring) {
    return true;
  }

  /**
   * Filter {@link GekozenPartij}. Als de methode false terug geeft dan is het het idee dat de geven gekozenPartij niet terug wordt gegeven.
   *
   * @param gekozenPartij om te controleren
   * @return Als true terug geeft dan wordt de GekozenPartij meegenomen door de eml lezer.
   */
  default boolean filter(final Partij partij) {
    return true;
  }

  /**
   * Filter {@link PartijStemmen}. Als de methode false terug geeft dan is het het idee dat de geven partijStemmen niet terug wordt gegeven.
   *
   * @param partijStemmen om te controleren
   * @return Als true terug geeft dan wordt de PartijStemmen meegenomen door de eml lezer.
   */
  default boolean filter(final PartijStemmen partijStemmen) {
    return true;
  }

  /**
   * Filter {@link RapportageGebied}. Als de methode false terug geeft dan is het het idee dat het geven rapportgebied niet terug wordt gegeven.
   *
   * @param rapportageGebied Rapportgebied om te controleren
   * @return Als true terug geeft dan wordt het rapportgebied meegenomen door de eml lezer.
   */
  default boolean filter(final RapportageGebied rapportageGebied) {
    return true;
  }

  /**
   * Filter {@link Regio}. Als de methode false terug geeft dan is het het idee dat de geven Regio niet terug wordt gegeven.
   *
   * @param Regio om te controleren
   * @return Als true terug geeft dan wordt de regio meegenomen door de eml lezer.
   */
  default boolean filter(final Regio regio) {
    return true;
  }

  /**
   * Filter {@link StemStatistieken}. Als de methode false terug geeft dan is het het idee dat de geven stemStatistieken niet terug wordt gegeven.
   *
   * @param stemStatistieken om te controleren
   * @return Als true terug geeft dan wordt de stemStatistieken meegenomen door de eml lezer.
   */
  default boolean filter(final StemStatistieken stemStatistieken) {
    return true;
  }

  /**
   * Filter {@link Verkiezing}. Als de methode false terug geeft dan is het het idee dat de geven verkiezing niet terug wordt gegeven.
   *
   * @param verkiezing om te controleren
   * @return Als true terug geeft dan wordt de verkiezing meegenomen door de eml lezer.
   */
  default boolean filter(final Verkiezing verkiezing) {
    return true;
  }

  /**
   * Voeg het object toe als het filter true terug geeft.
   *
   * @param <T> type van het object
   * @param lijst lijst waaraan toe te voegen
   * @param object object om toe te voegen
   */
  default <T extends Filter> void voegToe(final List<T> lijst, final T object) {
    if (object.filter(this)) {
      lijst.add(object);
    }
  }
}
