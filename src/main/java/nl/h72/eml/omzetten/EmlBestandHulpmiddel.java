/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import de.ivu.elect.nl.eml.EmlReader;
import de.ivu.elect.nl.eml.EmlWrapper;
import de.ivu.wahl.wus.oasis.eml.EML;

/**
 * Hulpmiddel om een bestand of Java stream in te lezen en om te zetten naar een {@link EML} object.
 */
public final class EmlBestandHulpmiddel {

  public enum Voorvoegsel {
    /**
     * Voorvoegsel van EML110a bestanden.
     */
    VOORVOEGSEL_110A_BESTANDEN("Verkiezingsdefinitie"),
    /**
     * Voorvoegsel van EML230ba bestanden.
     */
    VOORVOEGSEL_230B_BESTANDEN("Kandidatenlijsten_"),
    /**
     * Voorvoegsels van EML510x bestanden.
     */
    VOORVOEGSEL_510_BESTANDEN("Telling_"),
    /**
     * Voorvoegsels van EML510d Totaaltelling bestand.
     */
    VOORVOEGSEL_510_TOTAAL_TELLINGEN_BESTAND("Totaaltelling_"),
    /**
     * Voorvoegsel van EML520 bestanden.
     */
    VOORVOEGSEL_520_BESTANDEN("Resultaat_");

    private final String voorvoegselTekst;

    Voorvoegsel(final String voorvoegsel) {
      this.voorvoegselTekst = voorvoegsel;
    }

    public String getVoorvoegsel() {
      return voorvoegselTekst;
    }
  }

  private EmlBestandHulpmiddel() {
    // Hulpmiddel class.
  }

  /**
   * Leest een bestand in en retourneert een {@link EML} object.
   *
   * @param bestand bestand dat ingelezen wordt
   * @return {@link EML} object
   * @throws IOException Fout indien bestand niet kon worden ingelezen
   */
  public static EML lees(final File bestand) throws IOException {
    try (InputStream is = Files.newInputStream(bestand.toPath())) {
      return lees(is);
    }
  }

  /**
   * Leest een {@link InputStream} uit en retourneert een {@link EML} object.
   *
   * @param is InputStream die omgezet wordt
   * @return {@link EML} object
   */
  public static EML lees(final InputStream is) {
    final EmlReader reader = new EmlReader();
    final EmlWrapper emlWrapper = reader.readEMlNoHash(is);

    return emlWrapper.getContent();
  }
}
