/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.ivu.elect.nl.eml.readable.AffiliationWrapper;
import de.ivu.elect.nl.eml.readable.Eml230CandidateWrapper;
import de.ivu.elect.nl.eml.readable.Eml230ContestWrapper;
import de.ivu.elect.nl.eml.readable.Eml230Wrapper;
import de.ivu.wahl.wus.oasis.eml.EML;
import de.ivu.wahl.wus.oasis.eml.ElectionIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.kiesraad.ElectionDomain;

import nl.h72.eml.records.Eml230b;
import nl.h72.eml.records.Kandidaat;
import nl.h72.eml.records.Kieskring;
import nl.h72.eml.records.Partij;

/**
 * Leest de informatie uit een {@link EML} object en zet het om naar een {@link Eml230b} object.
 */
public final class Eml230bOmzetter {

  private final EmlFilter filter;

  public Eml230bOmzetter() {
    this(new EmlFilter() {
    });
  }

  public Eml230bOmzetter(final EmlFilter filter) {
    this.filter = filter;
  }

  /**
   * Leest de informatie uit een {@link EML} object en zet het om naar een {@link Eml230b} object.
   *
   * @param eml {@link EML} object
   * @return {@link Eml230b} object
   */
  public Eml230b zetOm(final EML eml) {
    final Eml230Wrapper wrapper = new Eml230Wrapper(eml);
    final ElectionIdentifierStructure eis = wrapper.getElectionIdentifierStructure();
    final ElectionDomain electionDomain = wrapper.getOrCreateElectionDomain();
    return new Eml230b(eml.getId(), Hulpmiddel.autoriteit(wrapper.getManagingAuthority().getContent()),
        Hulpmiddel.verkiezing(eis, wrapper.getElectionDate(), electionDomain), kieskringen(wrapper, electionDomain.getValue()));
  }

  private List<Kieskring> kieskringen(final Eml230Wrapper wrapper, final String verkiezingsDomain) {
    final List<Kieskring> kieskringen = new ArrayList<>();

    for (final Eml230ContestWrapper contest : wrapper.getContest()) {
      final List<Partij> partijen = new ArrayList<>();

      for (final AffiliationWrapper aff : contest.getAffiliation()) {
        final List<Kandidaat> kandidaten = new ArrayList<>();

        for (final Eml230CandidateWrapper candidate : aff.getCandidate()) {
          // @formatter:off
          final Kandidaat kandidaat = new Kandidaat(
              Integer.parseInt(candidate.getId()),
              Hulpmiddel.geslacht(candidate.getGender()),
              candidate.getInitials(),
              candidate.getFirstName(),
              Optional.ofNullable(candidate.getNamePrefix()).orElse(""),
              candidate.getLastName(),
              candidate.getLocalityName(),
              candidate.getCountryNameCode()
              );
          // @formatter:on
          filter.voegToe(kandidaten, kandidaat);
        }
        filter.voegToe(partijen, new Partij(Integer.parseInt(aff.getId()), aff.getRegisteredName(), aff.getListSubMissionMode(), kandidaten));
      }
      filter.voegToe(kieskringen, new Kieskring(contest.getId(), kieskringNaam(contest.getContestName(), verkiezingsDomain), partijen));
    }
    return kieskringen;
  }

  private static String kieskringNaam(final String contestName, final String electionName) {
    return contestName == null ? electionName : contestName;
  }
}
