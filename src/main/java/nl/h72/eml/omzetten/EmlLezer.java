/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ivu.wahl.wus.oasis.eml.EML;

import nl.h72.eml.records.LogItem;

/**
 * Class om EML bestanden in te lezen en aan de {@link EmlVerwerker} door te geven om verder te verwerken. Een instantie van de {@link EmlLezer} kan
 * worden gemaakt met de {@link EmlLezerBouwer}.
 */
public final class EmlLezer {
  /**
   * Interface die gebruikt kan worden om aanroepen van elk verwerk te wrappen. Dit wordt gebruikt om het mogelijk te maken om bestanden parallel te
   * verwerken.
   */
  public interface EmlLezerTaakVerwerker extends Consumer<Runnable> {
  }

  private static final Logger LOGGER = LoggerFactory.getLogger(EmlLezer.class);

  private final Eml110aOmzetter eml110aOmzetter;
  private final Eml230bOmzetter eml230bOmzetter;
  private final Eml510Omzetter eml510Omzetter;
  private final Eml520Omzetter eml520Omzetter;

  private final Consumer<LogItem> logItemLogger;
  private final EmlVerwerker emlVerwerker;
  private final EmlLezerTaakVerwerker taakVerwerker;

  private EmlLezer(final EmlVerwerker emlVerwerker, final EmlLezerTaakVerwerker taakVerwerker, final EmlFilter filter,
      final Consumer<LogItem> logItemLogger) {
    this.emlVerwerker = emlVerwerker;
    this.taakVerwerker = taakVerwerker;
    this.logItemLogger = logItemLogger;
    eml110aOmzetter = new Eml110aOmzetter(filter);
    eml230bOmzetter = new Eml230bOmzetter(filter);
    eml510Omzetter = new Eml510Omzetter(filter);
    eml520Omzetter = new Eml520Omzetter(filter);
  }

  /**
   * Maak een {@link EmlLezerBouwer} en initialiseer die met gegeven {@link EmlVerwerker}.
   *
   * @return Nieuwe {@link EmlLezerBouwer} om een EmlLezer mee te construeren.
   */
  public static EmlLezerBouwer bouwer(final EmlVerwerker verwerker) {
    return new EmlLezerBouwer(verwerker);
  }

  /**
   * Verwerkt alle eml.xml bestanden recursief in de opgegeven directory. Als een bestand niet kan worden ingelezen wordt dit gelogd en wordt het
   * bestand genegeerd.
   *
   * @param directory directory om bestanden te doorzoeken.
   * @return Geeft een lijst met verantwoording van ingelezen bestanden, of problemen met bestanden terug.
   */
  public void verwerk(final Path directory) {
    verwerk(directory, d -> true);
  }

  /**
   * Verwerkt alle eml.xml bestanden recursief in de opgegeven directory die ook voldoen aan het bestandsFilter. Dit kan bijvoorbeeld gebruikt worden
   * om alleen specifieke EML versies van bestanden in te lezen. Als een bestand niet kan worden ingelezen wordt dit gelogd en wordt het bestand
   * genegeerd.
   *
   * @param directory directory om bestanden te doorzoeken.
   */
  public void verwerk(final Path directory, final Predicate<File> bestandsFilter) {
    final int basisDirectoryLengte = directory.toFile().getAbsolutePath().length() + 1;

    try (final Stream<Path> stream = Files.walk(directory)) {
      stream.filter(file -> file.toFile().getName().endsWith(".eml.xml") && bestandsFilter.test(file.toFile()))
          .forEach(file -> taakVerwerker.accept(() -> verwerkBestand(file.toFile(), stripBasisDirectory(basisDirectoryLengte, file))));
    } catch (final IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private void verwerkBestand(final File bestand, final String relatieveBestandsnaam) {
    try {
      verwerk(emlVerwerker, bestand);
      logItemLogger.accept(new LogItem(bestand, true, "Bestand '" + relatieveBestandsnaam + "' ingelezen", null));
    } catch (final Exception e) {
      LOGGER.error("Fout bij verwerken bestand: {}", bestand, e);
      logItemLogger.accept(new LogItem(bestand, false, "Foutmelding bij inlezen bestand: '" + relatieveBestandsnaam + "':" + e.getMessage(), e));
    }
  }

  private static String stripBasisDirectory(final int basisDirectoryLengte, final Path bestand) {
    return bestand.toFile().getAbsolutePath().substring(basisDirectoryLengte);
  }

  /**
   * Verwerk het opgegeven bestand.
   *
   * @param verwerker Verwerker om ingelezen bestand mee te verwerken
   * @param bestand bestand om in te lezen
   * @throws Exception Exceptie als er problemen waren met het verwerken van het bestand
   * @throws IllegalArgumentException Als EML Id niet werd herkend
   */
  public void verwerk(final EmlVerwerker verwerker, final File bestand) throws Exception {
    final EML eml = EmlBestandHulpmiddel.lees(bestand);

    switch (eml.getId()) {
      case "110a":
        verwerker.verwerk(bestand, eml110aOmzetter.zetOm(eml));
        break;
      case "230b":
        verwerker.verwerk(bestand, eml230bOmzetter.zetOm(eml));
        break;
      case "510b", "510c", "510d":
        verwerker.verwerk(bestand, eml510Omzetter.zetOm(eml));
        break;
      case "520":
        verwerker.verwerk(bestand, eml520Omzetter.zetOm(eml));
        break;
      default:
        throw new IllegalArgumentException("Onbekend EML formaat in bestand met EML id " + eml.getId());
    }
  }

  /**
   * Class om {@link EmlLezer} mee samen te stellen.
   */
  public static class EmlLezerBouwer {
    private final EmlVerwerker emlVerwerker;
    private EmlFilter filter = new EmlFilter() {
    };
    private Consumer<LogItem> logItemLogger = lgr -> {
    };
    private EmlLezerTaakVerwerker taakVerwerker = Runnable::run;

    /**
     * Initialiseer met een {@link EmlVerwerker} object.
     *
     * @param emlVerwerker {@link EmlVerwerker} object
     */
    public EmlLezerBouwer(final EmlVerwerker emlVerwerker) {
      this.emlVerwerker = emlVerwerker;
    }

    /**
     * Voer een {@link EmlFilter} filter toe om EML510x resultaten uit te filteren.
     *
     * @param filter {@link EmlFilter}
     */
    public EmlLezerBouwer filter(final EmlFilter filter) {
      this.filter = filter;
      return this;
    }

    /**
     * Een {@link Consumer} waaraan na elke verwerken aan een bestand wordt meegegeven of het verwerken gelukt was of dat er een fout was opgetreden
     * en welke fout dat was.
     *
     * @param logItemLogger logger om verwerk succes/falen aan door te geven
     */
    public EmlLezerBouwer logger(final Consumer<LogItem> logItemLogger) {
      this.logItemLogger = logItemLogger;
      return this;
    }

    /**
     * Voeg een eigen taak verwerker toe. Hiermee kan elk
     *
     * @param taakVerwerker
     */
    public EmlLezerBouwer verwerker(final EmlLezerTaakVerwerker taakVerwerker) {
      this.taakVerwerker = taakVerwerker;
      return this;
    }

    /**
     * Construeer het {@link EmlLezer} object.
     *
     * @return nieuw {@link EmlLezer} object
     */
    public EmlLezer bouw() {
      return new EmlLezer(emlVerwerker, taakVerwerker, filter, logItemLogger);
    }
  }
}
