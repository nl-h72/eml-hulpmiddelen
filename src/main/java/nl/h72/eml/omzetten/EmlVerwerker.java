/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.io.File;

import nl.h72.eml.records.Eml110a;
import nl.h72.eml.records.Eml230b;
import nl.h72.eml.records.Eml510;
import nl.h72.eml.records.Eml520;

/**
 * Interface gebruikt in {@link EmlLezer} waar specifieke ingelezen EML bestanden worden doorgeven om verder te verwerken.
 */
@SuppressWarnings("java:S112")
public interface EmlVerwerker {

  /**
   * EML 110a gegevens.
   *
   * @param bestand Het bestand waar de gegevens uit werden gelezen
   * @param eml110a Het EML110a object
   */
  default void verwerk(final File bestand, final Eml110a eml110a) throws Exception {
  }

  /**
   * EML 230 gegevens.
   *
   * @param bestand Het bestand waar de gegevens uit werden gelezen
   * @param eml230 Het EML230 object
   */
  default void verwerk(final File bestand, final Eml230b eml230) throws Exception {
  }

  /**
   * EML 510 gegevens.
   *
   * @param bestand Het bestand waar de gegevens uit werden gelezen
   * @param eml510 Het EML510 object
   */
  default void verwerk(final File bestand, final Eml510 eml510) throws Exception {
  }

  /**
   * EML 520 gegevens.
   *
   * @param bestand Het bestand waar de gegevens uit werden gelezen
   * @param eml520 Het EML520 object
   */
  default void verwerk(final File bestand, final Eml520 eml520) throws Exception {
  }
}
