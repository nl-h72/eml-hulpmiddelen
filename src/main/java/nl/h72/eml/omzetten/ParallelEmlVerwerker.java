/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.h72.eml.omzetten.EmlLezer.EmlLezerBouwer;
import nl.h72.eml.omzetten.EmlLezer.EmlLezerTaakVerwerker;

/**
 * Met deze class kan het inlezen van EML bestanden parallel worden gedaan. Hierdoor kunnen de resultaten sneller worden verwerkt.
 *
 *
 * <code>
 * private static final List<String> PREFIX_510 = List.of("Totaaltelling_", "Telling_");
 *
 *   // Het volgende stuk code toont hoe deze ParallelEmlVerwerker kan worden gebruikt
 *   // om eml510 bestanden gelijktijdig te verwerken.
 *
 *   ...
 *   EmlVerwerker verwerker = ... // Initialiseer met eigen implementatie van EmlVerwerker interface.
 *
 *   // Maak een nieuwe instantie aan met de verwerker, die 3 processen gelijktijdig kan afhandelen.
 *   final ParallelEmlVerwerker parallelEmlLezer = new ParallelEmlVerwerker(verwerker, 3);
 *   // Start the verwerker met de specifieke taak die moet worden uitgevoerd.
 *   parallelEmlLezer.verwerk(bouwer -> verwerkEml510(bouwer, directory));
 *   ...
 *
 *  // Deze methode wordt in het in parallel verwerker uitgevoerd met een bouwer die de eml lezer initialiseert
 *  // om gelijktijdig meerdere bestanden te kunnen verwerken.
 *  // Deze methode moet de eml lezer aanmaken en vervolgens verwerk aanroepen om het proces te starten.
 * private static void verwerkEml510(final EmlLezerBouwer bouwer, final Path directory) {
 *   final EmlLezer emlLezer = bouwer.logger(System.out::println).bouw();
 *
 *   EmlBestandHulpmiddel.VOORVOEGSEL_510_BESTANDEN.forEach(voorvoegsel ->  emlLezer.verwerk(directory, f -> filterBestanden(f, voorvoegsel)));
 * }
 * </code>
 */
public class ParallelEmlVerwerker {

  /**
   * Deze interface is om een {@link EmlLezer} die parallel moet worden uitgevoerd te starten. In de implementatie van de start methode moet de
   * {@link EmlLezer} worden gebouwd met de geven bouwer. Vervolgens moet de {@link EmlLezer#verwerk} worden aangeroepen.
   */
  public interface ParallelleEmlLezer {
    /**
     * Roep in de implementatie van deze methode verwerk aan van de {@link EmlLezer} die met de meegeven bouwer gemaakt moet worden.
     *
     * @param bouwer Bouwer om {@link EmlLezer} mee te bouwen.
     */
    void start(EmlLezerBouwer bouwer);
  }

  private static final long WACHTRIJ_VERTRAGING_MS = 1000;
  private static final Logger LOGGER = LoggerFactory.getLogger(ParallelEmlVerwerker.class);

  private final Queue<Future<?>> wachtrij = new ArrayDeque<>();
  private final EmlVerwerker emlVerwerker;
  private final int aantalParallel;

  /**
   * Initialiseer de class met de gewenste {@link EmlVerwerker} en het aantal parallel uit te voeren processen.
   *
   * @param emlVerwerker te gebruiken EML verwerker
   * @param aantalParallel aantal parallelle processen
   */
  public ParallelEmlVerwerker(final EmlVerwerker emlVerwerker, final int aantalParallel) {
    this.emlVerwerker = emlVerwerker;
    this.aantalParallel = aantalParallel;
  }

  /**
   * Start en verwerk de parallelle Eml lezer.
   *
   * @param parallelleEmlLezer
   */
  public void verwerk(final ParallelleEmlLezer parallelleEmlLezer) {
    final ExecutorService executorService = Executors.newFixedThreadPool(aantalParallel);

    try {
      final AtomicBoolean klaar = new AtomicBoolean(false);
      final Thread wachtRijTaak = maakWachtrijThread(klaar);

      wachtRijTaak.setDaemon(true);
      wachtRijTaak.start();
      final EmlLezerTaakVerwerker verwerker = v -> wachtrij.add(executorService.submit(v));
      final EmlLezerBouwer bouwer = EmlLezer.bouwer(emlVerwerker).verwerker(verwerker);
      parallelleEmlLezer.start(bouwer);
      klaar.set(true);
      stopWachtrij(wachtRijTaak);
    } finally {
      executorService.shutdown();
    }
  }

  private Thread maakWachtrijThread(final AtomicBoolean klaar) {
    return new Thread(() -> {
      try {
        while (!klaar.get() || !wachtrij.isEmpty()) {
          if (!wachtrij.isEmpty()) {
            wachtrij.poll().get();
          } else if (!klaar.get()) {
            Thread.sleep(WACHTRIJ_VERTRAGING_MS);
          }
        }
      } catch (final InterruptedException e) {
        Thread.currentThread().interrupt();
      } catch (final ExecutionException e) {
        LOGGER.error("Wachtrij onderbroken", e);
      }
    });
  }

  private void stopWachtrij(final Thread wachtRijTaak) {
    try {
      wachtRijTaak.join();
    } catch (final InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }
}
