/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import java.util.Optional;

import de.ivu.wahl.wus.oasis.eml.AuthorityIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.ElectionIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.GenderType;
import de.ivu.wahl.wus.oasis.eml.ManagingAuthorityStructure;
import de.ivu.wahl.wus.oasis.eml.kiesraad.ElectionDomain;
import nl.h72.eml.records.Autoriteit;
import nl.h72.eml.records.Verkiezing;

/**
 * Class met hulpmiddel methoden die door meerdere omzetters gebruikt kunnen worden.
 */
final class Hulpmiddel {

  private Hulpmiddel() {
    // Hulpmiddel class
  }

  /**
   * Zet {@link ManagingAuthorityStructure} in een {@link Autoriteit} record.
   *
   * @param managingAuthority object om gegevens uit te halen
   * @return Autoriteit record
   */
  public static Autoriteit autoriteit(final ManagingAuthorityStructure managingAuthority) {
    final AuthorityIdentifierStructure authorityIdentifier = managingAuthority.getAuthorityIdentifier();

    return new Autoriteit(authorityIdentifier.getId(), authorityIdentifier.getValue());
  }

  /**
   * Vult een {@link Verkiezing} record.
   *
   * @param eis {@link ElectionIdentifierStructure} object
   * @param verkiezingsDatum datum verkiezing
   * @param verkiezingsDomain
   * @return
   */
  public static Verkiezing verkiezing(final ElectionIdentifierStructure eis, final String verkiezingsDatum, final ElectionDomain verkiezingsDomain) {
    return verkiezing(eis.getId(), eis.getElectionName(), eis.getElectionCategory(), verkiezingsDatum, verkiezingsDomain);
  }

  /**
   * Vult een {@link Verkiezing} record.
   *
   * @param verkiezingId id van de verkiezing
   * @param naam naam van de verkiezing
   * @param categorie categorie van de verkiezing
   * @param datum datum van de verkiezing
   * @param domein verkiezingsdomein
   * @return
   */
  public static Verkiezing verkiezing(final String verkiezingId, final String naam, final String categorie, final String datum,
      final ElectionDomain domein) {
    final Integer verkiezingsDomainId = domein == null || domein.getId() == null ? null : Integer.valueOf(domein.getId());
    final String verkiezingsDomainNaam = domein == null ? null : domein.getValue();

    return new Verkiezing(verkiezingId, naam, datum, categorie, verkiezingsDomainId, verkiezingsDomainNaam);
  }

  /**
   * Geeft een Nederlanse tekst voor geslacht. Geeft vraagteken als onbekend.
   *
   * @param gender gender als uit het EML bestand komt
   * @return Nederlands woord voor gender of vraagteken indien onbekend
   */
  public static String geslacht(final GenderType gender) {
    switch (Optional.ofNullable(gender).orElse(GenderType.UNKNOWN)) {
      case FEMALE:
        return "vrouw";
      case MALE:
        return "man";
      case UNKNOWN:
      default:
        return "?";
    }
  }
}
