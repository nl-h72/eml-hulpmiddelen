/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.records;

import java.util.List;

/**
 * Object met de inhoud van een EML 520 bestand.
 * Een EML 520 bestand bevat de lijst en volgorde van de gekozen kandidaten.
 *
 * Een EML 520 bestand is als volgt opgebouwd:
 *
 * <pre>
 * &lt;EML>
 *   &lt;TransactionId>1&lt;/TransactionId>
 *   &lt;ManagingAuthority>
 *     &lt;AuthorityIdentifier Id="CSB">Fryslân&lt;/AuthorityIdentifier>
 *     &lt;AuthorityAddress>&lt;/AuthorityAddress>
 *   &lt;/ManagingAuthority>
 *   &lt;kr:CreationDateTime>2023-03-23T10:48:59.732&lt;/kr:CreationDateTime>
 *   &lt;ds:CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments">&lt;/ds:CanonicalizationMethod>
 *   &lt;Result>
 *     &lt;Election>
 *       &lt;ElectionIdentifier Id="PS2023_Fryslan">
 *         &lt;ElectionName>Provinciale Staten Fryslân 2023&lt;/ElectionName>
 *         &lt;ElectionCategory>PS&lt;/ElectionCategory>
 *         &lt;kr:ElectionSubcategory>PS1&lt;/kr:ElectionSubcategory>
 *         &lt;kr:ElectionDomain>Fryslân&lt;/kr:ElectionDomain>
 *         &lt;kr:ElectionDate>2023-03-15&lt;/kr:ElectionDate>
 *       &lt;/ElectionIdentifier>
 *       &lt;Contest>
 *         &lt;ContestIdentifier Id="geen">&lt;/ContestIdentifier>
 *         &lt;Selection>
 *           &lt;AffiliationIdentifier Id="1">
 *             &lt;RegisteredName>CDA&lt;/RegisteredName>
 *           &lt;/AffiliationIdentifier>
 *           &lt;Elected>yes&lt;/Elected>
 *         &lt;/Selection>
 *         &lt;Selection>
 *           &lt;Candidate>
 *             &lt;CandidateIdentifier Id="1">&lt;/CandidateIdentifier>
 *             &lt;CandidateFullName>
 *               &lt;xnl:PersonName>&lt;xnl:NameLine NameType="Initials">R.F.&lt;/xnl:NameLine>
 *               &lt;xnl:FirstName>Friso&lt;/xnl:FirstName>&lt;xnl:NamePrefix>&lt;/xnl:NamePrefix>
 *               &lt;xnl:LastName>Douwstra&lt;/xnl:LastName>&lt;/xnl:PersonName>
 *             &lt;/CandidateFullName>
 *             &lt;Gender>male&lt;/Gender>
 *             &lt;QualifyingAddress>
 *               &lt;xal:Locality>
 *                 &lt;xal:LocalityName>Leeuwarden&lt;/xal:LocalityName>
 *               &lt;/xal:Locality>
 *             &lt;/QualifyingAddress>
 *           &lt;/Candidate>
 *           &lt;Ranking>1&lt;/Ranking>
 *           &lt;Elected>yes&lt;/Elected>
 *         &lt;/Selection>
 *       &lt;/Contest>
 *     &lt;/Election>
 *   &lt;/Result>
 * &lt;/EML>
 * </pre>
 */
public record Eml520(String emlId, Autoriteit autoriteit, Verkiezing verkiezing, List<GekozenPartij> gekozenPartijen) {

}
