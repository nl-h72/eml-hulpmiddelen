/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.records;

import java.util.List;

/**
 * Object met de inhoud van een EML 110a bestand.
 * Een EML 110a bestand bevat de Veriezingsdefinitie.
 *
 * Een EML 110a bestand is als volgt opgebouwd:
 *
 * <pre>
 * &lt;EML>
 *   &lt;TransactionId>1&lt;/TransactionId>
 *   &lt;IssueDate>2023-02-03&lt;/IssueDate>
 *   &lt;kr:CreationDateTime>2023-02-03T17:56:29.780&lt;/kr:CreationDateTime>
 *   &lt;ElectionEvent>
 *       &lt;EventIdentifier/>
 *       &lt;Election>
 *           &lt;ElectionIdentifier Id="PS2023_Fryslan">
 *               &lt;ElectionName>Provinciale Staten Fryslân 2023&lt;/ElectionName>
 *               &lt;ElectionCategory>PS&lt;/ElectionCategory>
 *               &lt;kr:ElectionSubcategory>PS1&lt;/kr:ElectionSubcategory>
 *               &lt;kr:ElectionDomain>Fryslân&lt;/kr:ElectionDomain>
 *               &lt;kr:ElectionDate>2023-03-15&lt;/kr:ElectionDate>
 *               &lt;kr:NominationDate>2023-01-30&lt;/kr:NominationDate>
 *           &lt;/ElectionIdentifier>
 *           &lt;Contest>
 *               &lt;ContestIdentifier Id="geen"/>
 *               &lt;VotingMethod>SPV&lt;/VotingMethod>
 *               &lt;MaxVotes>&lt;/MaxVotes>
 *           &lt;/Contest>
 *           &lt;kr:NumberOfSeats>43&lt;/kr:NumberOfSeats>
 *           &lt;kr:PreferenceThreshold>25&lt;/kr:PreferenceThreshold>
 *           &lt;kr:ElectionTree>
 *               &lt;kr:Region RegionNumber="2" RegionCategory="PROVINCIE" FrysianExportAllowed="true">
 *                   &lt;kr:RegionName>Fryslân&lt;/kr:RegionName>
 *               &lt;/kr:Region>
 *               &lt;kr:Region RegionNumber="1" RegionCategory="KIESKRING" SuperiorRegionNumber="2" SuperiorRegionCategory="PROVINCIE">
 *                   &lt;kr:RegionName>Leeuwarden&lt;/kr:RegionName>
 *                   &lt;kr:Committee CommitteeCategory="CSB"/>
 *                   &lt;kr:Committee CommitteeCategory="HSB"/>
 *               &lt;/kr:Region>
 *               ...
 *           &lt;/kr:ElectionTree>
 *           &lt;kr:RegisteredParties>
 *               &lt;kr:RegisteredParty>
 *                   &lt;kr:RegisteredAppellation>CDA&lt;/kr:RegisteredAppellation>
 *               &lt;/kr:RegisteredParty>
 *               ...
 *           &lt;/kr:RegisteredParties>
 *       &lt;/Election>
 *   &lt;/ElectionEvent>
 * &lt;/EML>
 * </pre>
 */
public record Eml110a(String emlId, String kieskringId, Verkiezing verkiezing, int aantalZetels, int voorkeursdrempel, List<Regio> regios,
    List<String> geregistreerdePartijen) {
}
