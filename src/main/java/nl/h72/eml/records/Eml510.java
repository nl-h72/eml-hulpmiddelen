/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.records;

import java.util.List;

/**
 * Object met de inhoud van een EML 510b, 510c of 510d bestand.
 * Een EML 510 bestand bevat de uitslagen per kandidaat, alsmede statistieken over de stembiljetten..
 *
 * Een EML 510b bestand is als volgt opgebouwd:
 *
 * <pre>
 * &lt;EML>
 *   &lt;TransactionId>1&lt;/TransactionId>
 *   &lt;ManagingAuthority>
 *     &lt;AuthorityIdentifier Id="0096">Vlieland&lt;/AuthorityIdentifier>
 *     &lt;AuthorityAddress>&lt;/AuthorityAddress>
 *   &lt;/ManagingAuthority>
 *   &lt;kr:CreationDateTime>2023-03-16T13:48:27.407&lt;/kr:CreationDateTime>
 *   &lt;ds:CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments">&lt;/ds:CanonicalizationMethod>
 *   &lt;Count>
 *     &lt;EventIdentifier>&lt;/EventIdentifier>
 *     &lt;Election>
 *       &lt;ElectionIdentifier Id="PS2023_Fryslan">
 *         &lt;ElectionName>Provinciale Staten Fryslân 2023&lt;/ElectionName>
 *         &lt;ElectionCategory>PS&lt;/ElectionCategory>
 *         &lt;kr:ElectionSubcategory>PS1&lt;/kr:ElectionSubcategory>
 *         &lt;kr:ElectionDomain>Fryslân&lt;/kr:ElectionDomain>
 *         &lt;kr:ElectionDate>2023-03-15&lt;/kr:ElectionDate>
 *       &lt;/ElectionIdentifier>
 *       &lt;Contests>
 *         &lt;Contest>
 *           &lt;ContestIdentifier Id="geen">&lt;/ContestIdentifier>
 *           &lt;TotalVotes>
 *             &lt;Selection>
 *               &lt;Identifier Id="1">
 *                 &lt;RegisteredName>CDA&lt;/RegisteredName>
 *               &lt;/AffiliationIdentifier>
 *               &lt;ValidVotes>27&lt;/ValidVotes>
 *             &lt;/Selection>
 *             &lt;Selection>
 *               &lt;Candidate>
 *                 &lt;CandidateIdentifier Id="1">&lt;/CandidateIdentifier>
 *               &lt;/Candidate>
 *               &lt;ValidVotes>20&lt;/ValidVotes>
 *             &lt;/Selection>
 *             &lt;Cast>960&lt;/Cast>
 *             &lt;TotalCounted>617&lt;/TotalCounted>
 *             &lt;RejectedVotes ReasonCode="ongeldig">0&lt;/RejectedVotes>
 *             &lt;RejectedVotes ReasonCode="blanco">4&lt;/RejectedVotes>
 *             &lt;UncountedVotes ReasonCode="geldige stempassen">519&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="geldige volmachtbewijzen">84&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="geldige kiezerspassen">18&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="toegelaten kiezers">621&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="meer getelde stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="minder getelde stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="meegenomen stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="te weinig uitgereikte stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="te veel uitgereikte stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="geen verklaring">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="andere verklaring">0&lt;/UncountedVotes>
 *           &lt;/TotalVotes>
 *           &lt;ReportingUnitVotes>
 *             &lt;ReportingUnitIdentifier Id="0096::SB1">Stembureau De Vliestroom&lt;/ReportingUnitIdentifier>
 *             &lt;Selection>
 *               &lt;AffiliationIdentifier Id="1">
 *                 &lt;RegisteredName>CDA&lt;/RegisteredName>
 *               &lt;/AffiliationIdentifier>
 *               &lt;ValidVotes>27&lt;/ValidVotes>
 *             &lt;/Selection>
 *             &lt;Selection>
 *               &lt;Candidate>
 *                 &lt;CandidateIdentifier Id="1">&lt;/CandidateIdentifier>
 *               &lt;/Candidate>
 *               &lt;ValidVotes>20&lt;/ValidVotes>
 *             &lt;/Selection>
 *             &lt;Cast>960&lt;/Cast>
 *             &lt;TotalCounted>617&lt;/TotalCounted>
 *             &lt;RejectedVotes ReasonCode="ongeldig">0&lt;/RejectedVotes>
 *             &lt;RejectedVotes ReasonCode="blanco">4&lt;/RejectedVotes>
 *             &lt;UncountedVotes ReasonCode="geldige stempassen">519&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="geldige volmachtbewijzen">84&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="geldige kiezerspassen">18&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="toegelaten kiezers">621&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="meer getelde stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="minder getelde stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="meegenomen stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="te weinig uitgereikte stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="te veel uitgereikte stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="geen verklaring">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="andere verklaring">0&lt;/UncountedVotes>
 *           &lt;/ReportingUnitVotes>
 *         &lt;/Contest>
 *       &lt;/Contests>
 *     &lt;/Election>
 *   &lt;/Count>
 * &lt;/EML>
 * </pre>
 *
 * Een EML 510d bestand is als volgt opgebouwd:
 *
 * <pre>
 * &lt;EML>
 *   &lt;TransactionId>1&lt;/TransactionId>
 *   &lt;ManagingAuthority>
 *     &lt;AuthorityIdentifier Id="CSB">Flevoland&lt;/AuthorityIdentifier>
 *     &lt;AuthorityAddress>&lt;/AuthorityAddress>
 *   &lt;/ManagingAuthority>
 *   &lt;kr:CreationDateTime>2023-03-22T10:19:21.684&lt;/kr:CreationDateTime>
 *   &lt;ds:CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments">&lt;/ds:CanonicalizationMethod>
 *   &lt;Count>
 *     &lt;EventIdentifier>&lt;/EventIdentifier>
 *     &lt;Election>
 *       &lt;ElectionIdentifier Id="PS2023_Flevoland">
 *         &lt;ElectionName>Provinciale Staten Flevoland 2023&lt;/ElectionName>
 *         &lt;ElectionCategory>PS&lt;/ElectionCategory>
 *         &lt;kr:ElectionSubcategory>PS1&lt;/kr:ElectionSubcategory>
 *         &lt;kr:ElectionDomain>Flevoland&lt;/kr:ElectionDomain>
 *         &lt;kr:ElectionDate>2023-03-15&lt;/kr:ElectionDate>
 *       &lt;/ElectionIdentifier>
 *       &lt;Contests>
 *         &lt;Contest>
 *           &lt;ContestIdentifier Id="geen">&lt;/ContestIdentifier>
 *           &lt;TotalVotes>
 *             &lt;Selection>
 *               &lt;AffiliationIdentifier Id="1">
 *                 &lt;RegisteredName>Forum voor Democratie&lt;/RegisteredName>
 *               &lt;/AffiliationIdentifier>&lt;ValidVotes>7343&lt;/ValidVotes>
 *             &lt;/Selection>
 *             &lt;Selection>
 *               &lt;Candidate>
 *                 &lt;CandidateIdentifier Id="1">&lt;/CandidateIdentifier>
 *               &lt;/Candidate>
 *               &lt;ValidVotes>3432&lt;/ValidVotes>
 *             &lt;/Selection>
 *             &lt;Selection>
 *               &lt;AffiliationIdentifier Id="1">...
 *             &lt;/Selection>
 *             &lt;Cast>313632&lt;/Cast>
 *             &lt;TotalCounted>163200&lt;/TotalCounted>
 *             &lt;RejectedVotes ReasonCode="ongeldig">457&lt;/RejectedVotes>
 *             &lt;RejectedVotes ReasonCode="blanco">588&lt;/RejectedVotes>
 *             &lt;UncountedVotes ReasonCode="geldige volmachtbewijzen">17516&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="geldige kiezerspassen">198&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="meer getelde stembiljetten">50&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="minder getelde stembiljetten">24&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="meegenomen stembiljetten">13&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="te weinig uitgereikte stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="te veel uitgereikte stembiljetten">22&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="geen verklaring">12&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="andere verklaring">10&lt;/UncountedVotes>
 *           &lt;/TotalVotes>
 *           &lt;ReportingUnitVotes>
 *             &lt;ReportingUnitIdentifier Id="0034">Almere&lt;/ReportingUnitIdentifier>
 *             &lt;Selection>
 *               &lt;AffiliationIdentifier Id="1">
 *                 &lt;RegisteredName>Forum voor Democratie&lt;/RegisteredName>
 *               &lt;/AffiliationIdentifier>
 *               &lt;ValidVotes>3172&lt;/ValidVotes>
 *             &lt;/Selection>
 *             &lt;Selection>
 *               &lt;Candidate>
 *                 &lt;CandidateIdentifier Id="1">&lt;/CandidateIdentifier>
 *               &lt;/Candidate>
 *               &lt;ValidVotes>1331&lt;/ValidVotes>
 *             &lt;/Selection>
 *             &lt;Selection>
 *               &lt;AffiliationIdentifier Id="2">
 *             &lt;/Selection>
 *             &lt;Cast>155563&lt;/Cast>
 *             &lt;TotalCounted>68949&lt;/TotalCounted>
 *             &lt;RejectedVotes ReasonCode="ongeldig">231&lt;/RejectedVotes>
 *             &lt;RejectedVotes ReasonCode="blanco">304&lt;/RejectedVotes>
 *             &lt;UncountedVotes ReasonCode="geldige volmachtbewijzen">7240&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="geldige kiezerspassen">121&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="meer getelde stembiljetten">32&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="minder getelde stembiljetten">8&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="meegenomen stembiljetten">6&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="te weinig uitgereikte stembiljetten">0&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="te veel uitgereikte stembiljetten">18&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="geen verklaring">2&lt;/UncountedVotes>
 *             &lt;UncountedVotes ReasonCode="andere verklaring">3&lt;/UncountedVotes>
 *           &lt;/ReportingUnitVotes>
 *         &lt;/Contest>
 *       &lt;/Contests>
 *     &lt;/Election>
 *   &lt;/Count>
 * &lt;/EML>
 * </pre>
 */
public record Eml510(String emlId, Autoriteit autoriteit, String kieskringId, Verkiezing verkiezing, RapportageGebied rapportageGebiedTotaal,
    List<RapportageGebied> rapportageGebieden) {
}
