/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.records;

/**
 * Generieke interface voor Kandidaat gegevens.
 */
public interface KandidaatGegevens {

  static final String NAAM_MET_ROEPNAAM_FORMAAT = "%s (%s) %s %s";
  static final String NAAM_FORMAAT = "%s %s %s";
  static final String NAAM_MET_ROEPNAAM_SORTEERBAAR_FORMAAT = "%s, %s (%s) %s";
  static final String NAAM_SORTEERBAAR_FORMAAT = "%s, %s %s";

  String roepnaam();

  String voorletters();

  String tussenvoegsel();

  String achternaam();

  String woonplaats();

  /**
   * @return Geeft de volledige naam van een kandidaat als een String terug:
   *         Voorletters (Roepnaam indien beschikbaar) Tussenvoegsel Achternaam.
   */
  default String volledigeNaam() {
    if (roepnaam() != null && !roepnaam().isBlank()) {
      return String.format(NAAM_MET_ROEPNAAM_FORMAAT, voorletters(), roepnaam(), tussenvoegsel(), achternaam());
    } else {
      return String.format(NAAM_FORMAAT, voorletters(), tussenvoegsel(), achternaam());
    }
  }

  /**
   * @return Geeft de volledige naam terug op een sorteerbare volgorde:
   *         Achternaam, Voorletters (Roepnaam indien beschikbaar) Tussenvoegsel
   */
  default String volledigeNaamAchternaamEerst() {
    if (roepnaam() != null && !roepnaam().isBlank()) {
      return String.format(NAAM_MET_ROEPNAAM_SORTEERBAAR_FORMAAT, achternaam(), voorletters(), roepnaam(), tussenvoegsel());
    } else {
      return String.format(NAAM_SORTEERBAAR_FORMAAT, achternaam(), voorletters(), tussenvoegsel());
    }
  }
}
