/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.records;

import java.math.BigInteger;
import java.util.Map;

import de.ivu.elect.nl.eml.readable.VoteType;
import nl.h72.eml.omzetten.EmlFilter;

/**
 * Statistieken van over het stemmen, met betrekking tot o.a. de stembiljetten.
 */
public record StemStatistieken(Map<VoteType, BigInteger> stemmenTypeTellingen) implements Filter {
  @Override
  public boolean filter(final EmlFilter filter) {
    return filter.filter(this);
  }

  public int getStemTelling(final VoteType voteType) {
    return stemmenTypeTellingen.getOrDefault(voteType, BigInteger.ZERO).intValue();
  }
}
