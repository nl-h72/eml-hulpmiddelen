//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.12.17 at 08:48:55 AM UTC 
//


package de.ivu.wahl.wus.oasis.eml.reportgenerator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Winner"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.kiesraad.nl/reportgenerator}ListOrCombinedList"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Looser"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://www.kiesraad.nl/reportgenerator}ListOrCombinedList"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element ref="{http://www.kiesraad.nl/reportgenerator}Allotting" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "winner",
    "looser",
    "allotting"
})
@XmlRootElement(name = "AbsoluteMajority")
public class AbsoluteMajority {

    @XmlElement(name = "Winner", required = true)
    protected AbsoluteMajority.Winner winner;
    @XmlElement(name = "Looser", required = true)
    protected AbsoluteMajority.Looser looser;
    @XmlElement(name = "Allotting")
    protected Allotting allotting;

    /**
     * Gets the value of the winner property.
     * 
     * @return
     *     possible object is
     *     {@link AbsoluteMajority.Winner }
     *     
     */
    public AbsoluteMajority.Winner getWinner() {
        return winner;
    }

    /**
     * Sets the value of the winner property.
     * 
     * @param value
     *     allowed object is
     *     {@link AbsoluteMajority.Winner }
     *     
     */
    public void setWinner(AbsoluteMajority.Winner value) {
        this.winner = value;
    }

    /**
     * Gets the value of the looser property.
     * 
     * @return
     *     possible object is
     *     {@link AbsoluteMajority.Looser }
     *     
     */
    public AbsoluteMajority.Looser getLooser() {
        return looser;
    }

    /**
     * Sets the value of the looser property.
     * 
     * @param value
     *     allowed object is
     *     {@link AbsoluteMajority.Looser }
     *     
     */
    public void setLooser(AbsoluteMajority.Looser value) {
        this.looser = value;
    }

    /**
     * Gets the value of the allotting property.
     * 
     * @return
     *     possible object is
     *     {@link Allotting }
     *     
     */
    public Allotting getAllotting() {
        return allotting;
    }

    /**
     * Sets the value of the allotting property.
     * 
     * @param value
     *     allowed object is
     *     {@link Allotting }
     *     
     */
    public void setAllotting(Allotting value) {
        this.allotting = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.kiesraad.nl/reportgenerator}ListOrCombinedList"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "listOrCombinedList"
    })
    public static class Looser {

        @XmlElement(name = "ListOrCombinedList", required = true)
        protected ListOrCombinedList listOrCombinedList;

        /**
         * Gets the value of the listOrCombinedList property.
         * 
         * @return
         *     possible object is
         *     {@link ListOrCombinedList }
         *     
         */
        public ListOrCombinedList getListOrCombinedList() {
            return listOrCombinedList;
        }

        /**
         * Sets the value of the listOrCombinedList property.
         * 
         * @param value
         *     allowed object is
         *     {@link ListOrCombinedList }
         *     
         */
        public void setListOrCombinedList(ListOrCombinedList value) {
            this.listOrCombinedList = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://www.kiesraad.nl/reportgenerator}ListOrCombinedList"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "listOrCombinedList"
    })
    public static class Winner {

        @XmlElement(name = "ListOrCombinedList", required = true)
        protected ListOrCombinedList listOrCombinedList;

        /**
         * Gets the value of the listOrCombinedList property.
         * 
         * @return
         *     possible object is
         *     {@link ListOrCombinedList }
         *     
         */
        public ListOrCombinedList getListOrCombinedList() {
            return listOrCombinedList;
        }

        /**
         * Sets the value of the listOrCombinedList property.
         * 
         * @param value
         *     allowed object is
         *     {@link ListOrCombinedList }
         *     
         */
        public void setListOrCombinedList(ListOrCombinedList value) {
            this.listOrCombinedList = value;
        }

    }

}
