//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.12.17 at 08:48:55 AM UTC 
//


package de.ivu.wahl.wus.oasis.eml.reportgenerator;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AnomalyInSeatDistribution" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.kiesraad.nl/reportgenerator}ListGroupAndResults" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "anomalyInSeatDistribution",
    "listGroupAndResults"
})
@XmlRootElement(name = "OverviewOfCandidatesAndResults")
public class OverviewOfCandidatesAndResults {

    @XmlElement(name = "AnomalyInSeatDistribution")
    protected List<String> anomalyInSeatDistribution;
    @XmlElement(name = "ListGroupAndResults", required = true)
    protected List<ListGroupAndResults> listGroupAndResults;

    /**
     * Gets the value of the anomalyInSeatDistribution property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the anomalyInSeatDistribution property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnomalyInSeatDistribution().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAnomalyInSeatDistribution() {
        if (anomalyInSeatDistribution == null) {
            anomalyInSeatDistribution = new ArrayList<String>();
        }
        return this.anomalyInSeatDistribution;
    }

    /**
     * Gets the value of the listGroupAndResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listGroupAndResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListGroupAndResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListGroupAndResults }
     * 
     * 
     */
    public List<ListGroupAndResults> getListGroupAndResults() {
        if (listGroupAndResults == null) {
            listGroupAndResults = new ArrayList<ListGroupAndResults>();
        }
        return this.listGroupAndResults;
    }

    public void setAnomalyInSeatDistribution(List<String> value) {
        this.anomalyInSeatDistribution = null;
        if (value!= null) {
            List<String> draftl = this.getAnomalyInSeatDistribution();
            draftl.addAll(value);
        }
    }

    public void setListGroupAndResults(List<ListGroupAndResults> value) {
        this.listGroupAndResults = null;
        if (value!= null) {
            List<ListGroupAndResults> draftl = this.getListGroupAndResults();
            draftl.addAll(value);
        }
    }

}
