package de.ivu.wahl.wus.oasis.eml;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "candidate"
})
@XmlRootElement(name = "AddressBook", namespace = "")
public class AddressBook {

  @XmlElement(name = "Candidate", namespace = "")
  protected List<CandidateStructureKR> candidate;

  public List<CandidateStructureKR> getCandidate() {
    return candidate;
  }

  public void setCandidate(final List<CandidateStructureKR> candidate) {
    this.candidate = candidate;
  }
}
