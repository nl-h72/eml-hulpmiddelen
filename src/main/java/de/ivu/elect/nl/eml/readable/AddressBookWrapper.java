package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.AddressBook;

import java.util.List;
import java.util.stream.Collectors;

public class AddressBookWrapper extends AbstractWrapper<AddressBook> {

  public AddressBookWrapper(AddressBook addressBook) {
    super(addressBook);
  }

  public List<Eml230CandidateWrapper> getCandidates() {
    return getContent().getCandidate().stream().map(candidateStructureKR -> new Eml230CandidateWrapper(candidateStructureKR)).collect(Collectors.toList());
  }
}
