package de.ivu.elect.nl.eml.readable;

import java.io.Serializable;

public class IdShortCode implements Comparable<IdShortCode>, Serializable {

  private static final long serialVersionUID = 1L;

  private final Long id;

  private final String shortcode;

  public IdShortCode(Long id, String shortcode) {
    this.id = id;
    this.shortcode = shortcode;
  }

  public Long getId() {
    return id;
  }

  public String getShortcode() {
    return shortcode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    IdShortCode that = (IdShortCode) o;

    return shortcode != null ? shortcode.equals(that.shortcode) : that.shortcode == null;
  }

  @Override
  public int hashCode() {
    return shortcode != null ? shortcode.hashCode() : 0;
  }

  @Override
  public String toString() {
    return "IdShortCode{" +
        "id=" + id +
        ", shortcode='" + shortcode + '\'' +
        '}';
  }

  @Override
  public int compareTo(IdShortCode o) {

    if (o == null) {
      return -1;
    }

    if (this.getId() == null && o.getId() != null) {
      return 1;
    }
    if (this.getId() != null && o.getId() == null) {
      return -1;
    }

    if (this.getId() != null && o.getId() != null) {
      int result = Long.compare(this.getId(), o.getId());
      if (result != 0) {
        return result;
      }
    }

    if (this.getShortcode() == o.getShortcode()) {
      return 0;
    }
    if (this.getShortcode() == null) {
      return -1;
    }
    if (o.getShortcode() == null) {
      return 1;
    }
    return this.getShortcode().compareTo(o.getShortcode());
  }
}
