package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.EML;

public class Eml110bWrapper extends AbstractEml110Wrapper {

  public Eml110bWrapper(EML content) {
    super(content);
    if (getContent().getManagingAuthority() == null) {
      getContent().setManagingAuthority(of.createManagingAuthorityStructure());
    }
    if (getContent().getManagingAuthority().getAuthorityAddress() == null) {
      getContent().getManagingAuthority().setAuthorityAddress(of.createAuthorityAddressStructure());
    }
  }
  public ManagingAuthorityWrapper getManagingAuthority() {
    return new ManagingAuthorityWrapper(getContent().getManagingAuthority());
  }

  public Eml110bContestWrapper getContest() {
    return new Eml110bContestWrapper(getContent().getElectionEvent().getElection().getContest());
  }

}
