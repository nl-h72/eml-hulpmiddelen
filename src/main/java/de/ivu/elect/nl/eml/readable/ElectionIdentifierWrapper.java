package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.ElectionIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.ObjectFactory;

public class ElectionIdentifierWrapper extends AbstractWrapper<ElectionIdentifierStructure> {
  public ElectionIdentifierWrapper() {
    this(new ObjectFactory().createElectionIdentifierStructure());

  }

  public ElectionIdentifierWrapper(ElectionIdentifierStructure content) {
    super(content);
  }
}
