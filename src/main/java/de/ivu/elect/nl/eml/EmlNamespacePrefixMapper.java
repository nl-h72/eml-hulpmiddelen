package de.ivu.elect.nl.eml;

import java.util.HashMap;
import java.util.Map;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class EmlNamespacePrefixMapper extends NamespacePrefixMapper {

  private Map<String, String> namespaceMap = new HashMap<>();

  public EmlNamespacePrefixMapper() {
    namespaceMap.put(XMLTags.NS_DS, XMLTags.NS_PREFIX_DS);
    namespaceMap.put(XMLTags.NS_ED, XMLTags.NS_PREFIX_ED);
    // namespaceMap.put(XMLTags.NS_EML, XMLTags.NS_PREFIX_EML);
    namespaceMap.put(XMLTags.NS_KR, XMLTags.NS_PREFIX_KR);
    namespaceMap.put(XMLTags.NS_RG, XMLTags.NS_PREFIX_RG);
    namespaceMap.put(XMLTags.NS_XAL, XMLTags.NS_PREFIX_XAL);
    namespaceMap.put(XMLTags.NS_XNL, XMLTags.NS_PREFIX_XNL);
    namespaceMap.put(XMLTags.NS_XSI, XMLTags.NS_PREFIX_XSI);
  }

  @Override
  public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
    return namespaceMap.getOrDefault(namespaceUri, suggestion);
  }
}