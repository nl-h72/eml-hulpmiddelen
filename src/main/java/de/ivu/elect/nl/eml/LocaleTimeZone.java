package de.ivu.elect.nl.eml;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

public interface LocaleTimeZone {

  Locale LOCALE = new Locale("nl", "NL");

  TimeZone TIME_ZONE = TimeZone.getTimeZone("Europe/Berlin");


  static DateFormat createDateFormat(String patttern) {
    SimpleDateFormat sdf = new SimpleDateFormat(patttern, LOCALE);
    sdf.setTimeZone(TIME_ZONE);
    return sdf;
  }
}
