package de.ivu.elect.nl.eml.readable;

import java.math.BigInteger;
import java.util.AbstractMap.SimpleEntry;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.ivu.wahl.wus.oasis.eml.AffiliationIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.CandidateIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.CandidateStructure;
import de.ivu.wahl.wus.oasis.eml.ReportingUnitVotes;

public abstract class AbstractVotesSectionWrapper<C> extends AbstractWrapper<C> {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractVotesSectionWrapper.class);

  protected final List<ReportingUnitVotes.Selection> sections;

  private final List<ReportingUnitVotes.RejectedVotes> rejectedVotes;

  private final List<ReportingUnitVotes.UncountedVotes> uncountedVotes;

  protected AbstractVotesSectionWrapper(C container, List<ReportingUnitVotes.Selection> sections, List<ReportingUnitVotes.RejectedVotes> rejectedVotes, List<ReportingUnitVotes.UncountedVotes> uncountedVotes) {
    super(container);
    this.sections = sections;
    this.rejectedVotes = rejectedVotes;
    this.uncountedVotes = uncountedVotes;
  }

  public Map<GroupVotesSection, Map<Map.Entry<String, String>, BigInteger>> getCandidateVotesPerGroup() {

    final Map<GroupVotesSection, Map<Map.Entry<String, String>, BigInteger>> result = new LinkedHashMap<>();

    Map<Map.Entry<String, String>, BigInteger> candidateVotes = null;

    for (final ReportingUnitVotes.Selection section : sections) {
      final AffiliationIdentifierStructure affiliationIdentifierStructure = getAffiliationIdentifierStructure(section);
      if (affiliationIdentifierStructure == null) {
        final CandidateStructure candidateStructure = getCandidateStructure(section);
        if (candidateVotes != null && candidateStructure != null && candidateStructure.getCandidateIdentifier() != null) {
          // Aanpassing: Gebruik ShortCode als id niet bestaat (Komt voor bij Totaaltellingen 2e kamer verkiezingen,
          // anders worden totaal tellingen niet meegenomen.
          final CandidateIdentifierStructure candidateIdentifier = candidateStructure.getCandidateIdentifier();
          final Map.Entry<String, String> id = new SimpleEntry(candidateIdentifier.getId(), candidateIdentifier.getShortCode());

          candidateVotes.put(id, getVotes(section));
        }
      } else {
        GroupVotesSection groupVotesSection = new GroupVotesSection(affiliationIdentifierStructure.getId(),
            affiliationIdentifierStructure.getRegisteredName(), getVotes(section), StringUtils.isBlank(affiliationIdentifierStructure.getRegisteredName()));
        candidateVotes = new LinkedHashMap<>();
        result.put(groupVotesSection, candidateVotes);
      }
    }

    return result;
  }

  public void setCandidateVotesPerGroup(Map<GroupVotesSection, Map<IdShortCode, BigInteger>> groupVotes, boolean useIdInTotalVote/*ELECTVAPP-1348*/,
                                        boolean useShortCode /* ELECTVAPP-1336 */) {
    for (Map.Entry<GroupVotesSection, Map<IdShortCode, BigInteger>> entry : groupVotes.entrySet()) {
      GroupVotesSection group = entry.getKey();
      Map<IdShortCode, BigInteger> candidateVotes = entry.getValue();
      ReportingUnitVotes.Selection selection = of.createReportingUnitVotesSelection();
      sections.add(selection);
      AffiliationIdentifierStructure affiliationIdentifierStructure = of.createAffiliationIdentifierStructure();
      selection.getContent().add(of.createAffiliationIdentifier(affiliationIdentifierStructure));
      affiliationIdentifierStructure.setId(group.getId());

      // ELECTVAPP-1745
      if (group.isBlancoList()) {
        affiliationIdentifierStructure.setRegisteredName("");
      } else {
        affiliationIdentifierStructure.setRegisteredName(group.getRegisteredName());
      }

      setVotes(selection, group.getVotes());

      candidateVotes.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(voteEntry-> {;
        IdShortCode key = voteEntry.getKey();
        ReportingUnitVotes.Selection candidateSelection = of.createReportingUnitVotesSelection();
        sections.add(candidateSelection);
        CandidateStructure candidateStructure = of.createCandidateStructure();
        candidateSelection.getContent().add(of.createCandidate(candidateStructure));
        candidateStructure.setCandidateIdentifier(of.createCandidateIdentifierStructure());

        if ((useIdInTotalVote || !isTotalVote()) && key.getId() != null) {
          candidateStructure.getCandidateIdentifier().setId(Long.toString(key.getId()));
        }
        if (useShortCode) {
          candidateStructure.getCandidateIdentifier().setShortCode(key.getShortcode());
        }
        setVotes(candidateSelection, voteEntry.getValue());
      });

    }
  }

  public abstract boolean isTotalVote();

  public Map<VoteType, BigInteger> getValidVotes() {
    Map<VoteType, BigInteger> result = new LinkedHashMap<>();
    result.put(VoteType.OPGEROEPENEN, getCast());
    result.put(VoteType.TOTAAL_GETELD, getTotalCounted());

    for (ReportingUnitVotes.RejectedVotes rejectedVote : rejectedVotes) {
      addVoteType(result, rejectedVote.getReasonCode(), rejectedVote.getValue());
    }
    for (ReportingUnitVotes.UncountedVotes uncountedVote : uncountedVotes) {
      addVoteType(result, uncountedVote.getReasonCode(), uncountedVote.getValue());
    }
    return result;
  }

  public void setValidVotes(Map<VoteType, BigInteger> validVotes) {

    setCast(validVotes.get(VoteType.OPGEROEPENEN));
    setTotalCounted(validVotes.get(VoteType.TOTAAL_GETELD));

    validVotes.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(e -> {

      VoteType key = e.getKey();
      BigInteger value = e.getValue();

      if (key.isRejectedVote()) {
        ReportingUnitVotes.RejectedVotes reportingUnitVotesRejectedVotes = of.createReportingUnitVotesRejectedVotes();
        rejectedVotes.add(reportingUnitVotesRejectedVotes);
        reportingUnitVotesRejectedVotes.setReasonCode(key.getReasonCode());
        reportingUnitVotesRejectedVotes.setValue(value);
      }
      if (key.isUncountedVote()) {
        ReportingUnitVotes.UncountedVotes reportingUnitVotesUncountedVotes = of.createReportingUnitVotesUncountedVotes();
        uncountedVotes.add(reportingUnitVotesUncountedVotes);
        reportingUnitVotesUncountedVotes.setReasonCode(key.getReasonCode());
        reportingUnitVotesUncountedVotes.setValue(value);
      }
    });

  }

  protected abstract void setTotalCounted(BigInteger value);

  protected abstract void setCast(BigInteger value);

  private void addVoteType(Map<VoteType, BigInteger> result, String reason, BigInteger value) {
    if (StringUtils.isNotBlank(reason) && value != null) {
      VoteType voteType = VoteType.fromReason(reason);
      if (voteType == null) {
        LOG.warn("Unbekannter Grund: '" + reason + "'");
      } else {
        result.put(voteType, value);
      }
    }
  }

  protected abstract BigInteger getTotalCounted();

  protected abstract BigInteger getCast();

  protected AffiliationIdentifierStructure getAffiliationIdentifierStructure(ReportingUnitVotes.Selection section) {
    return getContent(section).stream().filter(jaxbElement -> jaxbElement.getValue() instanceof AffiliationIdentifierStructure).findFirst()
        .map(jaxbElement -> (AffiliationIdentifierStructure) jaxbElement.getValue()).orElse(null);
  }

  protected CandidateStructure getCandidateStructure(ReportingUnitVotes.Selection section) {
    return getContent(section).stream().filter(jaxbElement -> jaxbElement.getValue() instanceof CandidateStructure).findFirst()
        .map(jaxbElement -> (CandidateStructure) jaxbElement.getValue()).orElse(null);
  }

  protected BigInteger getVotes(ReportingUnitVotes.Selection section) {
    return getContent(section).stream().filter(jaxbElement -> jaxbElement.getValue() instanceof BigInteger).findFirst()
        .map(jaxbElement -> (BigInteger) jaxbElement.getValue()).orElse(null);
  }

  private void setVotes(ReportingUnitVotes.Selection selection, BigInteger value) {
    selection.getContent().add(of.createReportingUnitVotesSelectionValidVotes(value));
  }

  protected List<JAXBElement<?>> getContent(ReportingUnitVotes.Selection section) {
    return section.getContent();
  }
}
