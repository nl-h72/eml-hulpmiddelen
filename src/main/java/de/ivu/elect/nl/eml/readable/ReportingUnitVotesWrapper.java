package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.ReportingUnitVotes;

public class ReportingUnitVotesWrapper extends AbstractWrapper<ReportingUnitVotes> {
  protected ReportingUnitVotesWrapper(ReportingUnitVotes content) {
    super(content);

    if (content.getReportingUnitIdentifier() == null) {
      content.setReportingUnitIdentifier(of.createReportingUnitIdentifierStructure());
    }
  }

  public String getId() {
    return getContent().getReportingUnitIdentifier().getId();
  }

  public void setId(String id) {
    getContent().getReportingUnitIdentifier().setId(id);
  }

  public String getName() {
    return getContent().getReportingUnitIdentifier().getValue();
  }

  public void setName(String name) {
    getContent().getReportingUnitIdentifier().setValue(name);
  }

  public ReportingUnitVotesSectionWrapper getVotesWrapper() {
    return new ReportingUnitVotesSectionWrapper(getContent());
  }

}
