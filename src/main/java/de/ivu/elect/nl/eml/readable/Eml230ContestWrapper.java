package de.ivu.elect.nl.eml.readable;

import java.util.Collections;
import java.util.List;

import de.ivu.wahl.wus.oasis.eml.CandidateList;
import de.ivu.wahl.wus.oasis.eml.ContestIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.ObjectFactory;


import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

public class Eml230ContestWrapper extends AbstractWrapper<CandidateList.Election.Contest>{

  public Eml230ContestWrapper(){
    super(new ObjectFactory().createCandidateListElectionContest());
  }

  protected Eml230ContestWrapper(CandidateList.Election.Contest content) {
    super(content);
  }


  public List<AffiliationWrapper> getAffiliation() {
    return getContent().getCandidateOrAffiliation().stream()
        .filter(o -> o instanceof CandidateList.Election.Contest.Affiliation).map(o -> (CandidateList.Election.Contest.Affiliation) o)
        .map(AffiliationWrapper::new)
        .collect(collectingAndThen(toList(), Collections::unmodifiableList));
  }

  public AffiliationWrapper addAffiliation(){
    AffiliationWrapper result = new AffiliationWrapper();
    getContent().getCandidateOrAffiliation().add(result.getContent());
    return result;
  }

  public String getId(){
    return getContestIdentifier().getId();
  }

  public void setId(String id){
    getContestIdentifier().setId(id);
  }

  public String getContestName(){
    return getContestIdentifier().getContestName();
  }

  public void setContestName(String contestName){
    getContestIdentifier().setContestName(contestName);
  }

  private ContestIdentifierStructure getContestIdentifier() {
    if (getContent().getContestIdentifier()==null){
      getContent().setContestIdentifier(of.createContestIdentifierStructure());
    }
    return getContent().getContestIdentifier();
  }

}
