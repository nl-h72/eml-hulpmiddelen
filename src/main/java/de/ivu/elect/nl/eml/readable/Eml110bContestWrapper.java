package de.ivu.elect.nl.eml.readable;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import de.ivu.wahl.wus.oasis.eml.ElectionEvent;
import de.ivu.wahl.wus.oasis.eml.PollingPlaceStructure110;

public class Eml110bContestWrapper extends AbstractWrapper<ElectionEvent.Election.Contest> {

  protected Eml110bContestWrapper(ElectionEvent.Election.Contest content) {
    super(content);
    if (getContent().getReportingUnit() == null) {
      getContent().setReportingUnit(of.createElectionEventElectionContestReportingUnit());
    }
    if (getContent().getReportingUnit().getReportingUnitIdentifier() == null) {
      getContent().getReportingUnit().setReportingUnitIdentifier(of.createReportingUnitIdentifierStructureKR());
    }
  }

  public String getUebergeordnetesGebietNummer() {
    return getContent().getReportingUnit().getReportingUnitIdentifier().getId();
  }

  public void setUebergeordnetesGebietNummer(String uebergeordnetesGebietNummer) {
    getContent().getReportingUnit().getReportingUnitIdentifier().setId(uebergeordnetesGebietNummer);
  }

  public String getUebergeordnetesGebietName() {
    return getContent().getReportingUnit().getReportingUnitIdentifier().getValue();
  }

  public void setUebergeordnetesGebietName(String uebergeordnetesGebietName) {
    getContent().getReportingUnit().getReportingUnitIdentifier().setValue(uebergeordnetesGebietName);
  }

  public List<PollingPlaceWrapper> getPollingPlace() {
    return getContent().getPollingPlace().stream().map(PollingPlaceWrapper::new).collect(Collectors.collectingAndThen(Collectors.toList(),
        Collections::unmodifiableList));
  }

  public PollingPlaceWrapper addPollingPlace() {
    PollingPlaceStructure110 pollingPlaceStructure110 = of.createPollingPlaceStructure110();
    getContent().getPollingPlace().add(pollingPlaceStructure110);
    return new PollingPlaceWrapper(pollingPlaceStructure110);
  }

}
