package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.EML;
import de.ivu.wahl.wus.oasis.eml.ElectionIdentifierStructure110A;
import de.ivu.wahl.wus.oasis.eml.ObjectFactory;
import de.ivu.wahl.wus.oasis.eml.VotingMethodType;

public abstract class AbstractEml110Wrapper extends AbstractWrapper<EML> {

  protected AbstractEml110Wrapper(EML eml) {
    super(eml);

    if (eml.getElectionEvent() == null) {
      eml.setElectionEvent(of.createElectionEvent());
    }
    if (eml.getElectionEvent().getEventIdentifier() == null) {
      eml.getElectionEvent().setEventIdentifier(new ObjectFactory().createEventIdentifierStructure());
    }
    if (eml.getElectionEvent().getElection() == null) {
      eml.getElectionEvent().setElection(of.createElectionEventElection());
    }

    if (getContent().getElectionEvent().getElection().getContest() == null) {
      getContent().getElectionEvent().getElection().setContest(of.createElectionEventElectionContest());
    }

    if (eml.getElectionEvent().getElection().getContest().getContestIdentifier()==null){
      eml.getElectionEvent().getElection().getContest().setContestIdentifier(new ObjectFactory().createElectionEventElectionContestContestIdentifier());
    }
    if (eml.getElectionEvent().getElection().getContest().getVotingMethod()==null) {
      eml.getElectionEvent().getElection().getContest().setVotingMethod(VotingMethodType.SPV /*war in allen Beispieldateien immer so*/);
    }
    if (eml.getElectionEvent().getElection().getContest().getMaxVotes()==null) {
      eml.getElectionEvent().getElection().getContest().setMaxVotes("");
    }
  }

  public ElectionIdentifierStructure110A getElectionIdentifier110a() {
    ElectionIdentifierStructure110A electionIdentifier = getContent().getElectionEvent().getElection().getElectionIdentifier();
    if (electionIdentifier == null) {
      electionIdentifier = of.createElectionIdentifierStructure110A();
      getContent().getElectionEvent().getElection().setElectionIdentifier(electionIdentifier);
    }
    return electionIdentifier;
  }

}
