package de.ivu.elect.nl.eml.readable;

import java.math.BigInteger;

public class GroupVotesSection {

  private final String id;

  private final String registeredName;

  private BigInteger votes;

  private final boolean blancoList;

  public GroupVotesSection(String id, String registeredName, BigInteger votes, boolean blancoList) {
    this.id = id;
    this.registeredName = registeredName;
    this.votes = votes;
    this.blancoList = blancoList;
  }

  public String getId() {
    return id;
  }

  public String getRegisteredName() {
    return registeredName;
  }

  public BigInteger getVotes() {
    return votes;
  }

  public void setVotes(BigInteger votes) {
    this.votes = votes;
  }

  public boolean isBlancoList() {
    return blancoList;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    GroupVotesSection that = (GroupVotesSection) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public String toString() {
    return "GroupVotesSection{" +
        "id='" + id + '\'' +
        ", registeredName='" + registeredName + '\'' +
        ", votes=" + votes +
        ", blancoList=" + blancoList +
        '}';
  }
}
