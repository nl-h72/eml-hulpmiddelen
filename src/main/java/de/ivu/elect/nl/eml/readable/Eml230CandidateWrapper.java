package de.ivu.elect.nl.eml.readable;

import java.util.Collections;

import javax.annotation.Nullable;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;

import de.ivu.wahl.wus.oasis.eml.CandidateIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.CandidateStructureKR;
import de.ivu.wahl.wus.oasis.eml.GenderType;
import de.ivu.wahl.wus.oasis.eml.MailingAddressStructure;
import de.ivu.wahl.wus.oasis.eml.ObjectFactory;
import de.ivu.wahl.wus.oasis.eml.QualifyingAddressStructure;
import de.ivu.wahl.wus.oasis.eml.external.xnl.PersonName;


import static org.apache.commons.lang3.StringUtils.isBlank;

public class Eml230CandidateWrapper extends AbstractEml230PersonWrapper<CandidateStructureKR> {

  public Eml230CandidateWrapper() {
    super(new ObjectFactory().createCandidateStructureKR());
  }

  protected Eml230CandidateWrapper(CandidateStructureKR content) {
    super(content);
  }

  private CandidateIdentifierStructure getCandidateIdentifier() {
    if (getContent().getCandidateIdentifier() == null) {
      getContent().setCandidateIdentifier(of.createCandidateIdentifierStructure());
    }
    return getContent().getCandidateIdentifier();
  }

  public String getId() {
    return getCandidateIdentifier().getId();
  }

  public void setId(String id) {
    getCandidateIdentifier().setId(id);
  }

  public String getShortCode() {
    String shortCode = getCandidateIdentifier().getShortCode();
    if (isBlank(shortCode)) {
      return getCandidateIdentifier().getShortCodeType(); // in EML mal als Attribut und mal als Element abgelegt, keiner weiß so richtig, wieso...
    }
    return shortCode;
  }

  public void setShortCodeId(String shortCode) {
    getCandidateIdentifier().setShortCode(shortCode);
  }

  public void setShortCodeRef(String shortCode) {
    getCandidateIdentifier().setShortCodeType(shortCode); // in EML mal als Attribut und mal als Element abgelegt, keiner weiß so richtig, wieso...
  }

  public GenderType getGender() {
    return getContent().getGender();
  }

  public void setGender(GenderType gender) {
    getContent().setGender(gender);
  }

  protected QualifyingAddressStructure getQualifyingAddress() {
    if (getContent().getQualifyingAddress() == null) {
      getContent().setQualifyingAddress(of.createQualifyingAddressStructure());
    }
    return getContent().getQualifyingAddress();
  }

  @Override
  protected PersonName getPersonName() {

    if (getContent().getCandidateFullName() == null) {
      getContent().setCandidateFullName(of.createPersonNameStructure());
    }
    if (getContent().getCandidateFullName().getPersonName() == null) {
      getContent().getCandidateFullName().setPersonName(new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createPersonName());
    }
// ELECTVAPP-781 ELECTVAPP-782
//    if (getContent().getCandidateFullName().getPersonName().getFirstName().isEmpty()) {
//      getContent().getCandidateFullName().getPersonName().getFirstName()
//          .add(new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createPersonNameTypeFirstName());
//    }
//
//    if (getContent().getCandidateFullName().getPersonName().getLastName().isEmpty()) {
//      getContent().getCandidateFullName().getPersonName().getLastName()
//          .add(new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createPersonNameTypeLastName());
//    }
//
//    if (getContent().getCandidateFullName().getPersonName().getNameLine().isEmpty()) {
//      NameLineType nameLineType = new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createNameLineType();
//      nameLineType.setNameType(INITIALS);
//      getContent().getCandidateFullName().getPersonName().getNameLine().add(nameLineType);
//    }

    return getContent().getCandidateFullName().getPersonName();
  }

  public XMLGregorianCalendar getDateOfBirth() {
    return getContent().getDateOfBirth();
  }

  public void setDateOfBirth(XMLGregorianCalendar dateOfBirth) {
    getContent().setDateOfBirth(dateOfBirth);
  }

  public String getDateOfBirthAnnex() {
    return getContent().getDateOfBirthAnnex();
  }

  public void setDateOfBirthAnnex(String dateOfBirth) {
    getContent().setDateOfBirthAnnex(dateOfBirth);
  }

  @Override
  protected MailingAddressStructure getMailingAddress() {
    if (getContent().getContact() == null) {
      getContent().setContact(of.createContactDetailsStructureKR());
    }

    if (getContent().getContact().getMailingAddress() == null) {
      getContent().getContact().setMailingAddress(of.createMailingAddressStructure());
    }

    return getContent().getContact().getMailingAddress();
  }

  @Nullable
  public Eml230AgentWrapper getAgent() {
    if (getContent().getAgent() == null) {
      return null;
    }

    return new Eml230AgentWrapper(getContent().getAgent());
  }

  public Eml230AgentWrapper createAgent() {
    getContent().setAgent(of.createAgentStructureKR());
    return new Eml230AgentWrapper(getContent().getAgent());
  }

  public String getCountryNameCode() {
    return getCountryNameCodeFromCountry(getQualifyingAddress().getCountry());
  }

  // see also de.ivu.elect.nl.eml.readable.Eml520CandidateWrapper.setCountryNameCode
  public void setCountryNameCode(String countryNameCode, String localityName) {
    if (isBlank(countryNameCode)) {
      return;
    }

    QualifyingAddressStructure qualifyingAddress = getQualifyingAddress();

    if (qualifyingAddress.getCountry() == null) {
      qualifyingAddress.setCountry(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createAddressDetailsCountry());
    }

    if (qualifyingAddress.getCountry().getCountryNameCode().isEmpty()) {
      qualifyingAddress.getCountry()
          .setCountryNameCode(Collections.singletonList(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createAddressDetailsCountryCountryNameCode()));
    }


    // diese Einträge werden benötigt...
    if (qualifyingAddress.getCountry().getLocality()==null) {
      qualifyingAddress.getCountry().setLocality(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createLocality());
    }
    if (qualifyingAddress.getCountry().getLocality().getLocalityName().isEmpty()) {
      qualifyingAddress.getCountry().getLocality().getLocalityName().add(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createLocalityLocalityName());
    }

    qualifyingAddress.getCountry().getCountryNameCode().get(0).setContent(countryNameCode);
    if (StringUtils.isNotBlank(localityName)) {
      qualifyingAddress.getCountry().getLocality().getLocalityName().get(0).setContent(localityName); // ELECTVAPP-1110
    }
  }

  @Override
  public String getLocalityName() {

    QualifyingAddressStructure qualifyingAddress = getQualifyingAddress();
    if (qualifyingAddress.getLocality() == null && (qualifyingAddress.getCountry() == null || qualifyingAddress.getCountry().getLocality() == null)) {
      return null;
    }

    if (qualifyingAddress.getLocality()!=null) {
      if (!qualifyingAddress.getLocality().getLocalityName().isEmpty()) {
        return qualifyingAddress.getLocality().getLocalityName().get(0).getContent();
      }
    }
    if (qualifyingAddress.getCountry()!=null &&  qualifyingAddress.getCountry().getLocality()!=null &&  !qualifyingAddress.getCountry().getLocality().getLocalityName().isEmpty()) {
      return qualifyingAddress.getCountry().getLocality().getLocalityName().get(0).getContent();
    }

    return null;
  }

  @Override
  public void setLocalityName(String localityName) {
    getQualifyingAddress().setLocality(null);
    if (StringUtils.isNotBlank(localityName)) {
      getQualifyingAddress().setLocality(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createLocality());
      getQualifyingAddress().getLocality().getLocalityName().add(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createLocalityLocalityName());
      getQualifyingAddress().getLocality().getLocalityName().get(0).setContent(localityName);
    }
  }

}
