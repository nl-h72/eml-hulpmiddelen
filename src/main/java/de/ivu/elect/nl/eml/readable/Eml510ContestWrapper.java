package de.ivu.elect.nl.eml.readable;

import java.util.List;
import java.util.stream.Collectors;

import de.ivu.elect.nl.eml.XmlHelper;
import de.ivu.wahl.wus.oasis.eml.ContestIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.Count;
import de.ivu.wahl.wus.oasis.eml.ReportingUnitVotes;

public class Eml510ContestWrapper extends AbstractEml5xxContestWrapper<Count.Election.Contests.Contest>{


  protected Eml510ContestWrapper(Count.Election.Contests.Contest content) {
    super(content);
  }

  @Override
  protected ContestIdentifierStructure getContestIdentifierStructure(){
    return XmlHelper.getOrCreateElement(ContestIdentifierStructure.class, getContent().getContent(),
        () -> of.createContestIdentifier(of.createContestIdentifierStructure()));
  }


  public List<ReportingUnitVotesWrapper> getReportingUnitVotes() {
    return XmlHelper.getElements(ReportingUnitVotes.class, getContent().getContent()).stream().map(ReportingUnitVotesWrapper::new).collect(Collectors.toList());
  }

  public ReportingUnitVotesWrapper addReportingUnitVotes(boolean addToContent){
    ReportingUnitVotes reportingUnitVotes = of.createReportingUnitVotes();
    if (addToContent /*ELECTVAPP-1704*/) {
      getContent().getContent().add(reportingUnitVotes);
    }
    return new ReportingUnitVotesWrapper(reportingUnitVotes);
  }

  public List<TotalVotesWrapper> getTotalVotes() {
    return XmlHelper.getElements(Count.Election.Contests.Contest.TotalVotes.class, getContent().getContent()).stream().map(TotalVotesWrapper::new).collect(Collectors.toList());
  }

  public TotalVotesWrapper addTotalVotes(){
    Count.Election.Contests.Contest.TotalVotes totalVotes = of.createCountElectionContestsContestTotalVotes();
    getContent().getContent().add(of.createCountElectionContestsContestTotalVotes(totalVotes));
    return new TotalVotesWrapper(totalVotes);
  }

}
