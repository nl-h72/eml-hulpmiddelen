package de.ivu.elect.nl.eml.readable;

import java.util.Collections;

import org.apache.commons.lang3.StringUtils;

import de.ivu.wahl.wus.oasis.eml.CandidateIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.CandidateStructure;
import de.ivu.wahl.wus.oasis.eml.GenderType;
import de.ivu.wahl.wus.oasis.eml.PersonNameStructure;
import de.ivu.wahl.wus.oasis.eml.QualifyingAddressStructure;
import de.ivu.wahl.wus.oasis.eml.external.xnl.NameLineType;
import de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory;
import de.ivu.wahl.wus.oasis.eml.external.xnl.PersonName;
import de.ivu.wahl.wus.oasis.eml.external.xnl.PersonNameType;


import static org.apache.commons.lang3.StringUtils.isBlank;

public class Eml520CandidateWrapper extends AbstractWrapper<CandidateStructure> {
  protected Eml520CandidateWrapper(CandidateStructure content) {
    super(content);
    if (getContent().getCandidateIdentifier() == null) {
      getContent().setCandidateIdentifier(of.createCandidateIdentifierStructure());
    }
    if (getContent().getCandidateFullName() == null) {
      getContent().setCandidateFullName(of.createPersonNameStructure());
    }

    if (getContent().getCandidateFullName().getPersonName() == null) {
      getContent().getCandidateFullName().setPersonName(new ObjectFactory().createPersonName());
    }

    if (getContent().getCandidateFullName().getPersonName().getFirstName().isEmpty()) {
      getContent().getCandidateFullName().getPersonName().getFirstName()
          .add(new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createPersonNameTypeFirstName());
    }

    if (getContent().getCandidateFullName().getPersonName().getLastName().isEmpty()) {
      getContent().getCandidateFullName().getPersonName().getLastName()
          .add(new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createPersonNameTypeLastName());
    }

    if (getContent().getCandidateFullName().getPersonName().getNameLine().isEmpty()) {
      NameLineType nameLineType = new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createNameLineType();
      nameLineType.setNameType(Eml230CandidateWrapper.INITIALS);
      getContent().getCandidateFullName().getPersonName().getNameLine().add(nameLineType);
    }

    if (getContent().getQualifyingAddress() == null) {
      getContent().setQualifyingAddress(of.createQualifyingAddressStructure());
    }

    if (getContent().getQualifyingAddress().getLocality() == null) {
      getContent().getQualifyingAddress().setLocality(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createLocality());
    }

    if (getContent().getQualifyingAddress().getLocality().getLocalityName().isEmpty()) {
      getContent().getQualifyingAddress().getLocality().getLocalityName().add(
          new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createLocalityLocalityName());
    }

  }

  protected CandidateIdentifierStructure getCandidateIdentifier() {
    return getContent().getCandidateIdentifier();
  }

  protected PersonNameStructure getCandidateFullName() {
    return getContent().getCandidateFullName();
  }

  public void setId(String id) {
    getCandidateIdentifier().setId(id);
  }

  public void setShortCode(String shortCode) {
    getCandidateIdentifier().setShortCode(shortCode);
  }

  public void setInitials(String initials) {
    getPersonName().getNameLine().get(0).setContent(initials);
  }

  public void setLastName(String lastName) {
    getPersonName().getLastName().get(0).setContent(lastName);
  }

  public void setFirstName(String firstName) {
    getPersonName().getFirstName().get(0).setContent(firstName);
  }

  public void setNamePrefix(String namePrefix) {
    getNamePrefixNode().setContent(namePrefix);
  }

  private PersonNameType.NamePrefix getNamePrefixNode() {
    if (getPersonName().getNamePrefix()==null){
      getPersonName().setNamePrefix(new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createPersonNameTypeNamePrefix());
    }
    return getPersonName().getNamePrefix();
  }

  protected PersonName getPersonName() {
    return getCandidateFullName().getPersonName();
  }

  public void setGender(GenderType genderType) {
    getContent().setGender(genderType);
  }

  // ELECT-14252
//  public void setLocalityName(String localityName) {
//    getContent().getQualifyingAddress().getLocality().getLocalityName().get(0).setContent(localityName);
//  }

  // ELECT-14252
  public void setLocalityName(String localityName) {
    getQualifyingAddress().setLocality(null);
    if (StringUtils.isNotBlank(localityName)) {
      getQualifyingAddress().setLocality(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createLocality());
      getQualifyingAddress().getLocality().getLocalityName().add(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createLocalityLocalityName());
      getQualifyingAddress().getLocality().getLocalityName().get(0).setContent(localityName);
    }
  }


  // ELECT-14252 see also de.ivu.elect.nl.eml.readable.Eml230CandidateWrapper.setCountryNameCode
  public void setCountryNameCode(String countryNameCode, String localityName) {
    if (isBlank(countryNameCode)) {
      return;
    }

    QualifyingAddressStructure qualifyingAddress = getQualifyingAddress();

    if (qualifyingAddress.getCountry() == null) {
      qualifyingAddress.setCountry(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createAddressDetailsCountry());
    }

    if (qualifyingAddress.getCountry().getCountryNameCode().isEmpty()) {
      qualifyingAddress.getCountry()
          .setCountryNameCode(Collections.singletonList(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createAddressDetailsCountryCountryNameCode()));
    }


    // diese Einträge werden benötigt...
    if (qualifyingAddress.getCountry().getLocality()==null) {
      qualifyingAddress.getCountry().setLocality(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createLocality());
    }
    if (qualifyingAddress.getCountry().getLocality().getLocalityName().isEmpty()) {
      qualifyingAddress.getCountry().getLocality().getLocalityName().add(new oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory().createLocalityLocalityName());
    }

    qualifyingAddress.getCountry().getCountryNameCode().get(0).setContent(countryNameCode);
    if (StringUtils.isNotBlank(localityName)) {
      qualifyingAddress.getCountry().getLocality().getLocalityName().get(0).setContent(localityName); // ELECTVAPP-1110
    }
  }


  protected QualifyingAddressStructure getQualifyingAddress() {
    if (getContent().getQualifyingAddress() == null) {
      getContent().setQualifyingAddress(of.createQualifyingAddressStructure());
    }
    return getContent().getQualifyingAddress();
  }



}
