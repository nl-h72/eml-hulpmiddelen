package de.ivu.elect.nl.eml.readable;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import de.ivu.wahl.wus.oasis.eml.EML;
import de.ivu.wahl.wus.oasis.eml.ElectionEvent;
import de.ivu.wahl.wus.oasis.eml.kiesraad.ObjectFactory;
import de.ivu.wahl.wus.oasis.eml.kiesraad.Region;
import de.ivu.wahl.wus.oasis.eml.kiesraad.RegionCategoryType;
import de.ivu.wahl.wus.oasis.eml.kiesraad.RegisteredParty;

public class Eml110aWrapper extends AbstractEml110Wrapper {

  public Eml110aWrapper(EML eml) {
    super(eml);

    if (eml.getElectionEvent().getElection().getElectionTree() == null) {
      eml.getElectionEvent().getElection().setElectionTree(new ObjectFactory().createElectionTree());
    }

    if (eml.getElectionEvent().getElection().getElectionTree().getRegion() == null) {
      eml.getElectionEvent().getElection().getElectionTree().setRegion(new ArrayList<>());
    }

    if (eml.getElectionEvent().getElection().getRegisteredParties() == null) {
      eml.getElectionEvent().getElection().setRegisteredParties(new ObjectFactory().createRegisteredParties());
    }

    if (eml.getElectionEvent().getElection().getRegisteredParties().getRegisteredParty() == null) {
      eml.getElectionEvent().getElection().getRegisteredParties().setRegisteredParty(new ArrayList<>());
    }
  }

  public ElectionEvent.Election getElection() {
    return getContent().getElectionEvent().getElection();
  }

  public List<Region> getRegions() {
    return getElection().getElectionTree().getRegion();
  }

  public Region addRegion(Short nummer, RegionCategoryType type, String name, @Nullable Short uebergeordnetNummer,
                          @Nullable RegionCategoryType uebergeordnetType, boolean romanNumerals) {
    Region region = new ObjectFactory().createRegion();
    getRegions().add(region);
    region.setRegionNumber(nummer);
    region.setRegionName(name);
    region.setRegionCategory(type);

    region.setSuperiorRegionNumber(uebergeordnetNummer);
    region.setSuperiorRegionCategory(uebergeordnetType);

    if (romanNumerals) {
      region.setRomanNumerals(romanNumerals);
    }

    return region;
  }

  public List<RegisteredParty> getRegisteredParties() {
    return getElection().getRegisteredParties().getRegisteredParty();
  }

  public RegisteredParty addRegisteredParty(String name) {
    RegisteredParty registeredParty = new ObjectFactory().createRegisteredParty();
    registeredParty.setRegisteredAppellation(name);
    getRegisteredParties().add(registeredParty);
    return registeredParty;
  }
}
