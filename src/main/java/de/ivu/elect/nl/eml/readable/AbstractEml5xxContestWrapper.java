package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.ContestIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.Count;

public abstract class AbstractEml5xxContestWrapper<T> extends AbstractWrapper<T>{

  protected AbstractEml5xxContestWrapper(T content) {
    super(content);
  }

  protected abstract ContestIdentifierStructure getContestIdentifierStructure();

  public String getContestId() {
    return getContestIdentifierStructure().getId();
  }

  public void setContestId(String contestId) {
    getContestIdentifierStructure().setId(contestId);
  }

  public String getContestName(){
    return getContestIdentifierStructure().getContestName();
  }

  public void setContestName(String contestName){
    getContestIdentifierStructure().setContestName(contestName);
  }

}
