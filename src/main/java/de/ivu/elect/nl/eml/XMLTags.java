package de.ivu.elect.nl.eml;

public interface XMLTags {

  String NS_XSI = "http://www.w3.org/2001/XMLSchema-instance"; //$NON-NLS-1$
  String NS_EML = "urn:oasis:names:tc:evs:schema:eml"; //$NON-NLS-1$
  String NS_DS = "http://www.w3.org/2000/09/xmldsig#"; //$NON-NLS-1$
  String NS_RG = "http://www.kiesraad.nl/reportgenerator"; //$NON-NLS-1$
  String NS_KR = "http://www.kiesraad.nl/extensions"; //$NON-NLS-1$
  String NS_ED = "http://www.kiesraad.nl/electiondefinition"; //$NON-NLS-1$
  String NS_XNL = "urn:oasis:names:tc:ciq:xsdschema:xNL:2.0"; //$NON-NLS-1$
  String NS_XAL = "urn:oasis:names:tc:ciq:xsdschema:xAL:2.0"; //$NON-NLS-1$

  String NS_PREFIX_XSI = "xsi"; //$NON-NLS-1$
  String NS_PREFIX_EML = "eml"; //$NON-NLS-1$
  String NS_PREFIX_DS = "ds"; //$NON-NLS-1$
  String NS_PREFIX_RG = "rg"; //$NON-NLS-1$
  String NS_PREFIX_KR = "kr"; //$NON-NLS-1$
  String NS_PREFIX_ED = "ed"; //$NON-NLS-1$
  String NS_PREFIX_XNL = "xnl"; //$NON-NLS-1$
  String NS_PREFIX_XAL = "xal"; //$NON-NLS-1$

}
