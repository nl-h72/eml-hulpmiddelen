package de.ivu.elect.nl.eml.readable;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import de.ivu.wahl.wus.oasis.eml.ContestIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.Result;

public class Eml520ContestWrapper extends AbstractEml5xxContestWrapper<Result.Election.Contest> {

  protected Eml520ContestWrapper(Result.Election.Contest contest) {
    super(contest);

    if (getContent().getContestIdentifier() == null) {
      getContent().setContestIdentifier(of.createContestIdentifierStructure());
    }
  }

  @Override
  public ContestIdentifierStructure getContestIdentifierStructure() {
    return getContent().getContestIdentifier();
  }

  public List<Eml520SelectionWrapper> getSelections() {
    return getContent().getSelection().stream().map(Eml520SelectionWrapper::new).collect(Collectors.toList());
  }

  protected Eml520SelectionWrapper addSelection() {
    Result.Election.Contest.Selection selection = of.createResultElectionContestSelection();
    getContent().getSelection().add(selection);
    return new Eml520SelectionWrapper(selection);
  }


  public Eml520SelectionWrapper addElectedParty(String id, String name) {
    Eml520SelectionWrapper eml520SelectionWrapper = addSelection();
    eml520SelectionWrapper.addElectedParty(id, name);
    return eml520SelectionWrapper;
  }

  public Eml520CandidateWrapper addElectedCandidate(BigInteger ranking) {
    Eml520SelectionWrapper eml520SelectionWrapper = addSelection();
    return eml520SelectionWrapper.addElectedCandidate(ranking);
  }

}
