package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.PollingPlaceStructure110;

import de.ivu.wahl.wus.oasis.eml.VotingChannelType;
import oasis.names.tc.ciq.xsdschema.xal._2.ObjectFactory;

public class PollingPlaceWrapper extends AbstractWrapper<PollingPlaceStructure110> {
  protected PollingPlaceWrapper(PollingPlaceStructure110 content) {
    super(content);

    if (content.getPhysicalLocation() == null) {
      content.setPhysicalLocation(of.createPollingPlaceStructure110PhysicalLocation());
    }
    if (getContent().getPhysicalLocation().getAddress() == null) {
      getContent().getPhysicalLocation().setAddress(of.createPollingPlaceStructure110PhysicalLocationAddress());
    }
    if (getContent().getPhysicalLocation().getAddress().getLocality() == null) {
      getContent().getPhysicalLocation().getAddress().setLocality(new ObjectFactory().createLocalityType110());
    }
    if (getContent().getPhysicalLocation().getAddress().getLocality().getLocalityName() == null) {
      getContent().getPhysicalLocation().getAddress().getLocality().setLocalityName(new ObjectFactory().createLocalityNameType());
    }
    if (getContent().getPhysicalLocation().getPollingStation() == null) {
      getContent().getPhysicalLocation().setPollingStation(of.createPollingPlaceStructure110PhysicalLocationPollingStation());
    }

    if (getContent().getPhysicalLocation().getAddress() == null) {
      getContent().getPhysicalLocation().setAddress(of.createPollingPlaceStructure110PhysicalLocationAddress());
    }

    if (getContent().getPhysicalLocation().getAddress().getLocality() == null) {
      getContent().getPhysicalLocation().getAddress().setLocality(new ObjectFactory().createLocalityType110());
    }
    if (getContent().getPhysicalLocation().getAddress().getLocality().getPostalCode() == null) {
      getContent().getPhysicalLocation().getAddress().getLocality().setPostalCode(new ObjectFactory().createPostalCode());
    }

    if (getContent().getPhysicalLocation().getAddress().getLocality().getPostalCode().getPostalCodeNumber().isEmpty()) {
      getContent().getPhysicalLocation().getAddress().getLocality().getPostalCode().getPostalCodeNumber()
          .add(new ObjectFactory().createPostalCodePostalCodeNumber());
    }
  }

  public VotingChannelType getChannel() {
    return getContent().getChannel();
  }

  public void setChannel(VotingChannelType channelType) {
    getContent().setChannel(channelType);
  }

  public String getName() {
    return getContent().getPhysicalLocation().getAddress().getLocality().getLocalityName().getContent();
  }

  public void setName(String name) {
    getContent().getPhysicalLocation().getAddress().getLocality().getLocalityName().setContent(name);
  }

  public String getId() {
    return getContent().getPhysicalLocation().getPollingStation().getId();
  }

  public void setId(String id) {
    getContent().getPhysicalLocation().getPollingStation().setId(id);
  }

  public String getAnzahlWahlberechitige() {
    return getContent().getPhysicalLocation().getPollingStation().getValue();
  }

  public void setAnzahlWahlberechitige(String anzahlWahlberechtigte) {
    getContent().getPhysicalLocation().getPollingStation().setValue(anzahlWahlberechtigte);
  }

  public String getZipCode() {
    return getContent().getPhysicalLocation().getAddress().getLocality().getPostalCode().getPostalCodeNumber().get(0).getContent();
  }

  public void setZipCode(String zipCode) {
    getContent().getPhysicalLocation().getAddress().getLocality().getPostalCode().getPostalCodeNumber().get(0).setContent(zipCode);
  }

}
