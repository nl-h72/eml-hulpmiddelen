package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.Count;

public class TotalVotesWrapper extends AbstractWrapper<Count.Election.Contests.Contest.TotalVotes> {
  protected TotalVotesWrapper(Count.Election.Contests.Contest.TotalVotes content) {
    super(content);
  }

  public TotalVotesSectionWrapper getVotesWrapper() {
    return new TotalVotesSectionWrapper(getContent());
  }

}
