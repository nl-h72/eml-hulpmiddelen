package de.ivu.elect.nl.eml.readable;

import java.math.BigInteger;

import de.ivu.wahl.wus.oasis.eml.Count;

public class TotalVotesSectionWrapper extends AbstractVotesSectionWrapper<Count.Election.Contests.Contest.TotalVotes> {

  public TotalVotesSectionWrapper(Count.Election.Contests.Contest.TotalVotes container) {
    super(container, container.getSelection(), container.getRejectedVotes(), container.getUncountedVotes());
  }

  @Override
  public boolean isTotalVote() {
    return true;
  }

  @Override
  protected void setTotalCounted(BigInteger value) {
    getContent().setTotalCounted(value);
  }

  @Override
  protected void setCast(BigInteger value) {
    getContent().setCast(value);
  }

  @Override
  protected BigInteger getTotalCounted() {
    return getContent().getTotalCounted();
  }

  @Override
  protected BigInteger getCast() {
    return getContent().getCast();
  }

}
