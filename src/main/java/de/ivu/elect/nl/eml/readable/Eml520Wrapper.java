package de.ivu.elect.nl.eml.readable;

import java.util.List;
import java.util.stream.Collectors;

import de.ivu.wahl.wus.oasis.eml.EML;
import de.ivu.wahl.wus.oasis.eml.ElectionIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.Result;

public class Eml520Wrapper extends AbstractEmlElectionWrapper {

  public Eml520Wrapper(EML content) {
    super(content);

    if (getContent().getResult() == null) {
      getContent().setResult(of.createResult());
    }

    if (getContent().getResult().getElection().isEmpty()) {
      getContent().getResult().getElection().add(of.createResultElection());
    }

    if (getContent().getResult().getElection().get(0).getElectionIdentifier() == null) {
      getContent().getResult().getElection().get(0).setElectionIdentifier(of.createElectionIdentifierStructure());
    }

  }


  @Override
  public ElectionIdentifierStructure getElectionIdentifierStructure() {
    return getElection().getElectionIdentifier();
  }

  private Result.Election getElection() {
    return getContent().getResult().getElection().get(0);
  }

  public List<Eml520ContestWrapper> getContests() {
    return getElection().getContest().stream().map(Eml520ContestWrapper::new).collect(Collectors.toList());
  }

  public Eml520ContestWrapper addContest() {
    Result.Election.Contest resultElectionContest = of.createResultElectionContest();
    getElection().getContest().add(resultElectionContest);
    return new Eml520ContestWrapper(resultElectionContest);
  }

}
