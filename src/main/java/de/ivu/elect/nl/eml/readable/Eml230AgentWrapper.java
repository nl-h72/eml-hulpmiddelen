package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.AgentStructureKR;
import de.ivu.wahl.wus.oasis.eml.MailingAddressStructure;
import de.ivu.wahl.wus.oasis.eml.external.xnl.PersonName;
import de.ivu.wahl.wus.oasis.eml.kiesraad.LivingAddressType;
import de.ivu.wahl.wus.oasis.eml.kiesraad.ObjectFactory;

public class Eml230AgentWrapper extends AbstractEml230PersonWrapper<AgentStructureKR> {
  protected Eml230AgentWrapper(AgentStructureKR content) {
    super(content);
  }

  @Override
  protected PersonName getPersonName() {

    if (getContent().getAgentIdentifier() == null) {
      getContent().setAgentIdentifier(of.createAgentIdentifierStructure());
    }

    if (getContent().getAgentIdentifier().getAgentName() == null) {
      getContent().getAgentIdentifier().setAgentName(of.createPersonNameStructure());
    }
    if (getContent().getAgentIdentifier().getAgentName().getPersonName() == null) {
      getContent().getAgentIdentifier().getAgentName().setPersonName(new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createPersonName());
    }
// ELECTVAPP-781 ELECTVAPP-782
//    if (getContent().getAgentIdentifier().getAgentName().getPersonName().getFirstName().isEmpty()) {
//      getContent().getAgentIdentifier().getAgentName().getPersonName().getFirstName()
//          .add(new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createPersonNameTypeFirstName());
//    }
//
//    if (getContent().getAgentIdentifier().getAgentName().getPersonName().getLastName().isEmpty()) {
//      getContent().getAgentIdentifier().getAgentName().getPersonName().getLastName()
//          .add(new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createPersonNameTypeLastName());
//    }
//
//    if (getContent().getAgentIdentifier().getAgentName().getPersonName().getNameLine().isEmpty()) {
//      NameLineType nameLineType = new de.ivu.wahl.wus.oasis.eml.external.xnl.ObjectFactory().createNameLineType();
//      nameLineType.setNameType(INITIALS);
//      getContent().getAgentIdentifier().getAgentName().getPersonName().getNameLine().add(nameLineType);
//    }

    return getContent().getAgentIdentifier().getAgentName().getPersonName();
  }

  public String getRole(){
    return getContent().getRole();
  }

  public void setRole(String role){
    getContent().setRole(role);
  }

  @Override
  public String getLocalityName() {
    return getLivingAddress().getLocalityName();
  }

  @Override
  public void setLocalityName(String localityName) {
    getLivingAddress().setLocalityName(localityName);
  }

  private LivingAddressType getLivingAddress() {
    if (getContent().getLivingAddress() == null) {
      getContent().setLivingAddress(new ObjectFactory().createLivingAddressType());
    }
    return getContent().getLivingAddress();
  }

  @Override
  protected MailingAddressStructure getMailingAddress() {
    if (getContent().getContact() == null) {
      getContent().setContact(of.createContactDetailsStructureKR());
    }

    if (getContent().getContact().getMailingAddress() == null) {
      getContent().getContact().setMailingAddress(of.createMailingAddressStructure());
    }

    return getContent().getContact().getMailingAddress();
  }

}
