package de.ivu.elect.nl.eml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import de.ivu.elect.jee.exchange.JAXBUtil;
import de.ivu.elect.nl.eml.readable.AddressBookWrapper;
import de.ivu.wahl.wus.oasis.eml.AddressBook;

public class AddressBookReader {

   static final int BUFFER_SIZE = 1024 * 100;

  public AddressBookWrapper readAddressBook(InputStream is) {
    try {
      try (ByteArrayOutputStream bos = new ByteArrayOutputStream(BUFFER_SIZE)) {
        IOUtils.copy(is, bos);

        try (ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray())) {
          return new AddressBookWrapper(new JAXBUtil().unmarshal(AddressBook.class, bis));
        }
      }
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }
}
