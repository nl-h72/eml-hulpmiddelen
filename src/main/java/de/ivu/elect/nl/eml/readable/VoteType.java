package de.ivu.elect.nl.eml.readable;

import java.util.Arrays;

import javax.annotation.Nullable;

public enum VoteType {

  /*extra*/
  OPGEROEPENEN("opgeroepen"){
    @Override
    public boolean isRejectedVote() {
      return false;
    }

    @Override
    public boolean isUncountedVote() {
      return false;
    }
  },
  TOTAAL_GETELD("totaal geteld") {
    @Override
    public boolean isRejectedVote() {
      return false;
    }

    @Override
    public boolean isUncountedVote() {
      return false;
    }
  },

  /*rejected votes*/
  ONGELDIG("ongeldig"){
    @Override
    public boolean isRejectedVote() {
      return true;
    }

    @Override
    public boolean isUncountedVote() {
      return false;
    }
  },
  BLANCO("blanco"){
    @Override
    public boolean isRejectedVote() {
      return true;
    }

    @Override
    public boolean isUncountedVote() {
      return false;
    }

  },

  /*uncounted*/
  GELDIGE_STEMPASSEN("geldige stempassen"),
  GELDIGE_VOLMACHTBEWIJZEN("geldige volmachtbewijzen"),
  GELDIGE_KIEZERSPASSEN("geldige kiezerspassen"),
  TOEGELATEN_KIEZERS("toegelaten kiezers"),
  STEMBILJETTEN_MEER_GETELDE("meer getelde stembiljetten"),
  STEMBILJETTEN_MINDER_GETELDE("minder getelde stembiljetten"),
  STEMBILJETTEN_MEEGENOMEN("meegenomen stembiljetten"),
  STEMBILJETTEN_TE_WEINIG_UITGEREIKTE("te weinig uitgereikte stembiljetten"),
  STEMBILJETTEN_TE_VEEL_UITGEREIKTE("te veel uitgereikte stembiljetten"),
  STEMBILJETTEN_GEEN_VERKLARING("geen verklaring"),
  STEMBILJETTEN_ANDERE_VERKLARING("andere verklaring"),

  STEMBILJETTEN_KWIJTGERAAKTE("kwijtgeraakte stembiljetten"),
  BRIEFSTEMBILJETTEN_TE_VEEL("te veel briefstembiljetten"),
  BRIEFSTEMBILJETTEN_GEEN("geen briefstembiljetten")
  ;

  private final String reasonCode;

  VoteType(final String reasonCode) {
    this.reasonCode = reasonCode;
  }

  public String getReasonCode() {
    return reasonCode;
  }

  public boolean isRejectedVote() {
    return false;
  }

  public  boolean isUncountedVote() {
    return true;
  }

  @Nullable
  public static VoteType fromReason(final String reason) {
    return Arrays.stream(values()).filter(value -> value.getReasonCode().equalsIgnoreCase(reason)).findFirst().orElse(null);
  }
}
