package de.ivu.elect.nl.eml;

public class EmlConfig {

  private boolean useCanonicalize;

  private boolean prettyPrint;

  public EmlConfig(boolean useCanonicalize, boolean prettyPrint) {
    this.useCanonicalize = useCanonicalize;
    this.prettyPrint = prettyPrint;
  }

  public boolean isUseCanonicalize() {
    return useCanonicalize;
  }

  public void setUseCanonicalize(boolean useCanonicalize) {
    this.useCanonicalize = useCanonicalize;
  }

  public boolean isPrettyPrint() {
    return prettyPrint;
  }

  public void setPrettyPrint(boolean prettyPrint) {
    this.prettyPrint = prettyPrint;
  }
}
