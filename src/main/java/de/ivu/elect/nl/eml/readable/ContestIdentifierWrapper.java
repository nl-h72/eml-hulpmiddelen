package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.ContestIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.ObjectFactory;

public class ContestIdentifierWrapper extends AbstractWrapper<ContestIdentifierStructure> {


  public ContestIdentifierWrapper() {
    this(new ObjectFactory().createContestIdentifierStructure());
  }

  public ContestIdentifierWrapper(ContestIdentifierStructure content) {
    super(content);
  }
}
