package de.ivu.elect.nl.eml;

import java.io.InputStream;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import de.ivu.elect.jee.exchange.JAXBUtil;
import de.ivu.wahl.wus.oasis.eml.EML;

public class EmlReader {

  static final int BUFFER_SIZE = 1024 * 100;

  public EmlWrapper readEMlNoHash(final InputStream is) {
    return readEMl(is, null);
  }

  public EmlWrapper readEMlHash(final InputStream is, final Consumer<String> hashConsumer) {
    return readEMl(is, hashConsumer);
  }

  private EmlWrapper readEMl(final InputStream is, @Nullable final Consumer<String> hashConsumer) {
    try {
      return new EmlWrapper(new JAXBUtil().unmarshal(EML.class, is));
    } catch (final Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }
}
