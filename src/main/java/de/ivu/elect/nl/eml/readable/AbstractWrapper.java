package de.ivu.elect.nl.eml.readable;

import de.ivu.wahl.wus.oasis.eml.ObjectFactory;

public abstract class AbstractWrapper<T> {

  private final T content;

  protected final ObjectFactory of = new ObjectFactory();

  protected AbstractWrapper(T content) {
    this.content = content;
  }

  public T getContent() {
    return content;
  }



}
