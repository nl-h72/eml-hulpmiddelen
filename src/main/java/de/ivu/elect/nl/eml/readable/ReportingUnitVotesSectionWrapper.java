package de.ivu.elect.nl.eml.readable;

import java.math.BigInteger;

import de.ivu.wahl.wus.oasis.eml.ReportingUnitVotes;

public class ReportingUnitVotesSectionWrapper extends AbstractVotesSectionWrapper<ReportingUnitVotes> {

  public ReportingUnitVotesSectionWrapper(ReportingUnitVotes container) {
    super(container, container.getSelection(), container.getRejectedVotes(), container.getUncountedVotes());
  }

  @Override
  public boolean isTotalVote() {
    return false;
  }

  @Override
  protected void setTotalCounted(BigInteger value) {
    getContent().setTotalCounted(value);
  }

  @Override
  protected void setCast(BigInteger value) {
    getContent().setCast(value);
  }

  @Override
  protected BigInteger getTotalCounted() {
    return getContent().getTotalCounted();
  }

  @Override
  protected BigInteger getCast() {
    return getContent().getCast();
  }


}
