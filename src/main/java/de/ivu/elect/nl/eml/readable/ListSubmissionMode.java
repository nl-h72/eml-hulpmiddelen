package de.ivu.elect.nl.eml.readable;

import java.util.Arrays;

/**
 * Code aus OSV übernommen (EListSubmissionMode)
 */
public enum ListSubmissionMode {
  /**
   * The same list is valid for all (at least two) districts. a) The lists are entered for a party
   * centrally with all candidates on a common list (no varying end). b) All submitted lists for the
   * different districts contain exactly the same candidates (varying ends are all empty or all the
   * same).
   */
  SET_OF_LISTS("stel gelijkluidende lijsten"),

  /**
   * A single candidate list is entered by the party. This list is valid for a single electoral
   * district. The political group attends the election only for a single district
   */
  SINGLELIST("op zichzelf staande lijst"),

  /**
   * There are at least two lists which differ in at least one candidate position: Submitted lists
   * with at least one varying candidate. The common part of the lists may be empty or not.
   */
  LISTGROUP("lijstengroep"),


  UNKNOWN("");;

  private final String value;

  ListSubmissionMode(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public boolean isListGroup() {
    return this.equals(LISTGROUP);
  }

  public static ListSubmissionMode from(String s) {
    return Arrays.stream(values()).filter(listSubmissionMode -> listSubmissionMode.getValue().equalsIgnoreCase(s)).findFirst().orElse(UNKNOWN);
  }
}
