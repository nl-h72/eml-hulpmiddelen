package de.ivu.elect.nl.eml.readable;

import java.math.BigInteger;

import de.ivu.wahl.wus.oasis.eml.AffiliationIdentifierStructure;
import de.ivu.wahl.wus.oasis.eml.CandidateStructure;
import de.ivu.wahl.wus.oasis.eml.Result;
import de.ivu.wahl.wus.oasis.eml.YesNoType;

public class Eml520SelectionWrapper extends AbstractWrapper<Result.Election.Contest.Selection> {

  protected Eml520SelectionWrapper(Result.Election.Contest.Selection content) {
    super(content);
  }

  protected void addElectedParty(String id, String name) {
    AffiliationIdentifierStructure affiliationIdentifierStructure = of.createAffiliationIdentifierStructure();
    affiliationIdentifierStructure.setId(id);
    affiliationIdentifierStructure.setRegisteredName(name);
    getContent().getContent().add(of.createAffiliationIdentifier(affiliationIdentifierStructure));
    getContent().getContent().add(of.createResultElectionContestSelectionElected(YesNoType.YES));
  }

  protected Eml520CandidateWrapper addElectedCandidate(BigInteger ranking) {
    CandidateStructure candidateStructure = of.createCandidateStructure();
    getContent().getContent().add(of.createCandidate(candidateStructure));
    if (ranking != null) {
      getContent().getContent().add(of.createResultElectionContestSelectionRanking(ranking));
    }
    getContent().getContent().add(of.createResultElectionContestSelectionElected(YesNoType.YES));
    return new Eml520CandidateWrapper(candidateStructure);
  }

}
