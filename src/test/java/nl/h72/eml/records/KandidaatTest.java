/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.records;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class for record {@link Kandidaat}
 */
class KandidaatTest {

  private static final Kandidaat KANDIDAAT = new Kandidaat(1, "", "A.F.Th.", "Adri", "van der", "Heijden", "", "");
  private static final Kandidaat KANDIDAAT_ZONDER_ROEPNAAM = new Kandidaat(1, "", "A.F.Th.", "", "van der", "Heijden", "", "");

  @Test
  void testVolledigeNaam() {
    assertEquals("A.F.Th. (Adri) van der Heijden", KANDIDAAT.volledigeNaam(), "Niet de verwachte volledige naam met roepnaam");
    assertEquals("A.F.Th. van der Heijden", KANDIDAAT_ZONDER_ROEPNAAM.volledigeNaam(), "Niet de verwachte volledige naam");
  }

  @Test
  void testVolledigeNaamAchternaamEerst() {
    assertEquals("Heijden, A.F.Th. (Adri) van der", KANDIDAAT.volledigeNaamAchternaamEerst(),
        "Niet verwachte sorteerbare volledige naam met roepnaam");
    assertEquals("Heijden, A.F.Th. van der", KANDIDAAT_ZONDER_ROEPNAAM.volledigeNaamAchternaamEerst(), "Niet verwachte sorteerbare volledige naam");
  }
}
