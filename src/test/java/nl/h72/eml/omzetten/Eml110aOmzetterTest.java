/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.ivu.wahl.wus.oasis.eml.EML;
import de.ivu.wahl.wus.oasis.eml.kiesraad.RegionCategoryType;
import nl.h72.eml.records.Eml110a;
import nl.h72.eml.records.Regio;

/**
 * Test class for {@link Eml110aOmzetter}.
 */
class Eml110aOmzetterTest extends AbstractEmlOmzetter {

  @Test
  void testVormEmlOmNaarEml230Record() throws IOException {
    final EML eml = lees("Verkiezingsdefinitie_PS2023_Fryslan.eml.xml");
    final Eml110a eml110a = new Eml110aOmzetter().zetOm(eml);

    assertEquals("110a", eml110a.emlId(), "Invoer is niet herkent als eml110a bestand");
    assertVerkiezing(eml110a.verkiezing(), "PS2023_Fryslan", "Provinciale Staten Fryslân 2023", "PS");

    final List<Regio> regios = eml110a.regios();
    assertEquals(20, regios.size(), "Komt niet overeen met verwachte aantal regios");

    final Regio regio = regios.get(3);
    assertEquals(60, regio.regioNummer(), "Niet het verwachte region nummer");
    assertEquals("Ameland", regio.naam(), "Niet de verwachte regionaam");
    assertEquals(RegionCategoryType.GEMEENTE, regio.categorie(), "Niet de verwachte regio categorie");
    assertEquals(1, regio.ouderRegioNummer(), "Niet het verwachte ouder regio nummer");
    assertEquals(RegionCategoryType.KIESKRING, regio.ouderRegioCategorie(), "Niet de verwachte ouder regio categorie");
    assertTrue(regio.frieseExportToegestaan(), "Friese export is toegestaan");
    assertFalse(regio.romeinseCijfers(), "Geen romeinse cijfers");
    assertEquals(List.of(), regio.commissieCategorieen(), "Niet de verwachte commissie categorieen");

    assertEquals(17, eml110a.geregistreerdePartijen().size(), "Komt niet overeen met verwachte aantal geregistreerde partijen");
    assertEquals("SP (Socialistische Partij)", eml110a.geregistreerdePartijen().get(8), "Komt niet overeen met de naam van de geregistreerde partij");
  }
}
