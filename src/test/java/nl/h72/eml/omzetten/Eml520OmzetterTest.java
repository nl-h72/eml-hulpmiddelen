/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.ivu.wahl.wus.oasis.eml.EML;
import nl.h72.eml.records.Eml520;
import nl.h72.eml.records.GekozenKandidaat;
import nl.h72.eml.records.GekozenPartij;

/**
 * Test class for {@link Eml520Omzetter}.
 */
class Eml520OmzetterTest extends AbstractEmlOmzetter {

  @Test
  void testVormEmlOmNaarEml510bRecord() throws IOException {
    final EML eml = lees("Resultaat_PS2023_Fryslan.eml.xml");
    final Eml520 eml520 = new Eml520Omzetter().zetOm(eml);

    assertEquals("520", eml520.emlId(), "Invoer is niet herkent als 520 bestand");
    final List<GekozenPartij> gekozenPartijen = eml520.gekozenPartijen();
    assertEquals(14, gekozenPartijen.size(), "Aantal partijen klopt niet");
    assertEquals(43, gekozenPartijen.stream().mapToInt(p -> p.gekozenKandidaten().size()).sum(), "Totaal aantal gekozen kandidaten klopt niet");
    final GekozenPartij gekozenPartij = gekozenPartijen.get(3);
    assertEquals(4, gekozenPartij.partijId(), "Partij id klopt niet");
    assertEquals("VVD", gekozenPartij.naam(), "Partijnaam klopt niet");
    final List<GekozenKandidaat> gekozenKandidaten = gekozenPartij.gekozenKandidaten();
    assertEquals(3, gekozenKandidaten.size(), "Aantal gekozen kandidaten klopt niet");
    final GekozenKandidaat gekozenKandidaat = gekozenKandidaten.get(1);
    assertEquals(2, gekozenKandidaat.kandidaatId(), "Kandidaat id klopt niet");
    assertEquals("man", gekozenKandidaat.geslacht(), "Geslacht van kandidaat klopt niet");
    assertEquals("E.J.", gekozenKandidaat.voorletters(), "Voorletters van kandidaat kloppen niet");
    assertEquals("Eric", gekozenKandidaat.roepnaam(), "Roepnaam van kandidaat klopt niet");
    assertEquals("ter", gekozenKandidaat.tussenvoegsel(), "Tussenvoegsel van kandidaat klopt niet");
    assertEquals("Keurs", gekozenKandidaat.achternaam(), "Achternaam van kandidaat klopt niet");
    assertEquals("Drachten", gekozenKandidaat.woonplaats(), "Woonplaats van kandidaat klopt niet");
    assertTrue(gekozenKandidaat.gekozen(), "Wel/niet gekozen van kandidaat klopt niet");
    assertEquals(2, gekozenKandidaat.rangschikking(), "Rangschikking van kandidaat klopt niet");
  }
}
