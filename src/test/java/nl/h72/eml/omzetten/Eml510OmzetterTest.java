/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.ivu.wahl.wus.oasis.eml.EML;
import nl.h72.eml.records.Eml510;
import nl.h72.eml.records.KandidaatStemmen;
import nl.h72.eml.records.PartijStemmen;
import nl.h72.eml.records.RapportageGebied;

/**
 * Test class for {@link Eml510Omzetter}.
 */
class Eml510OmzetterTest extends AbstractEmlOmzetter {

  private final Eml510Omzetter eml510Omzetter = new Eml510Omzetter();

  @Test
  void testVormEmlOmNaarEml510bRecord() throws IOException {
    final EML eml = lees("Telling_PS2023_Fryslan_gemeente_Vlieland.eml.xml");
    final Eml510 eml510 = eml510Omzetter.zetOm(eml);

    assertEquals("510b", eml510.emlId(), "Invoer is niet herkent als 510b bestand");
    assertEquals(1, eml510.rapportageGebieden().size(), "Aantal stemgroepen klopt niet");
    final RapportageGebied stemmen = eml510.rapportageGebieden().get(0);
    assertEquals("0096::SB1", stemmen.id(), "Stempgroep id klopt niet");
    assertEquals("De Vliestroom", stemmen.naam(), "Stemgroepnaam klopt niet");
    final List<PartijStemmen> partijenStemmen = stemmen.partijenStemmen();
    assertEquals(17, partijenStemmen.size());
    final PartijStemmen partijStemmen = partijenStemmen.get(4);
    assertEquals(5, partijStemmen.partijId(), "Partijid klopt niet");
    assertEquals(26, partijStemmen.aantalStemmen(), "Aantal stemmen voor de partij klopt niet");
    final List<KandidaatStemmen> kandidatenStemmen = partijStemmen.kandidatenStemmen();
    assertEquals(50, kandidatenStemmen.size(), "Aantal kandidaten klopt niet");
    final KandidaatStemmen kandidaatStemmen = kandidatenStemmen.get(3);
    assertEquals(4, kandidaatStemmen.kandidaatId(), "Kandidaat id klopt niet");
    assertEquals(1, kandidaatStemmen.aantalStemmen(), "Aantal stemmen voor de kandidaat klopt niet");
  }

  @Test
  void testVormEmlOmNaarEml510dRecord() throws IOException {
    final EML eml = lees("Totaaltelling_PS2023_Flevoland_provincie_Flevoland.eml.xml");
    final Eml510 eml510 = eml510Omzetter.zetOm(eml);

    assertEquals("510d", eml510.emlId(), "Invoer is niet herkent als 510d bestand");
    assertEquals(6, eml510.rapportageGebieden().size(), "Aantal stemgroepen klopt niet");
    final RapportageGebied stemmen = eml510.rapportageGebieden().get(4);
    assertEquals("0303", stemmen.id(), "Stempgroep id klopt niet");
    assertEquals("Dronten", stemmen.naam(), "Stemgroepnaam klopt niet");
    final List<PartijStemmen> partijenStemmen = stemmen.partijenStemmen();
    assertEquals(17, partijenStemmen.size());
    final PartijStemmen partijStemmen = partijenStemmen.get(4);
    assertEquals(5, partijStemmen.partijId(), "Partijid klopt niet");
    assertEquals(1696, partijStemmen.aantalStemmen(), "Aantal stemmen voor de partij klopt niet");
    final List<KandidaatStemmen> kandidatenStemmen = partijStemmen.kandidatenStemmen();
    assertEquals(22, kandidatenStemmen.size(), "Aantal kandidaten klopt niet");
    final KandidaatStemmen kandidaatStemmen = kandidatenStemmen.get(10);
    assertEquals(11, kandidaatStemmen.kandidaatId(), "Kandidaat id klopt niet");
    assertEquals(38, kandidaatStemmen.aantalStemmen(), "Aantal stemmen voor de kandidaat klopt niet");
  }

  @Test
  void testVormEmlOmNaarEml510dTKRecord() throws IOException {
    final EML eml = lees("Totaaltelling_TK2021.eml.xml");
    final Eml510 eml510 = eml510Omzetter.zetOm(eml);

    assertEquals("510d", eml510.emlId(), "Invoer is niet herkent als 510d bestand");
    assertEquals("RutteM", eml510.rapportageGebiedTotaal().partijenStemmen().get(0).kandidatenStemmen().get(0).kandidaatCode(),
        "Kandidaat code voor eerste kandidaat totaal klopt niet");
    assertEquals(20, eml510.rapportageGebieden().size(), "Aantal stemgroepen klopt niet");
    final RapportageGebied stemmen = eml510.rapportageGebieden().get(4);
    assertEquals("HSB5", stemmen.id(), "Stempgroep id klopt niet");
    assertEquals("Kieskring Lelystad", stemmen.naam(), "Stemgroepnaam klopt niet");
    final List<PartijStemmen> partijenStemmen = stemmen.partijenStemmen();
    assertEquals(29, partijenStemmen.size());
    final PartijStemmen partijStemmen = partijenStemmen.get(4);
    assertEquals(5, partijStemmen.partijId(), "Partijid klopt niet");
    assertEquals(9979, partijStemmen.aantalStemmen(), "Aantal stemmen voor de partij klopt niet");
    final List<KandidaatStemmen> kandidatenStemmen = partijStemmen.kandidatenStemmen();
    assertEquals(50, kandidatenStemmen.size(), "Aantal kandidaten klopt niet");
    final KandidaatStemmen kandidaatStemmen = kandidatenStemmen.get(10);
    assertEquals(11, kandidaatStemmen.kandidaatId(), "Kandidaat id klopt niet");
    assertEquals(150, kandidaatStemmen.aantalStemmen(), "Aantal stemmen voor de kandidaat klopt niet");
  }
}
