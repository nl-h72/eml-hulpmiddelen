/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.io.InputStream;

import de.ivu.wahl.wus.oasis.eml.EML;

import nl.h72.eml.records.Verkiezing;

/**
 * Abstracte basis class voor unit tests.
 */
class AbstractEmlOmzetter {

  /**
   * Lees een bestand uit de zelfde directory als waaruit de test wordt opgestart.
   *
   * @param bestandsnaam naam van in te lezen bestand
   * @return ingeleze {@link EML} object
   * @throws IOException
   */
  protected EML lees(final String bestandsnaam) throws IOException {
    try (InputStream is = getClass().getResourceAsStream(bestandsnaam)) {
      assertNotNull(is, "Kon het bestand '" + bestandsnaam + "' niet vinden.");
      return EmlBestandHulpmiddel.lees(is);
    }
  }

  /**
   * Assert hulpmiddel om inhoud van {@link Verkiezing} object te controleren.
   *
   * @param verkiezing
   * @param id verwacht id
   * @param naam verwachte naam
   * @param categorie verwachte categorie
   */
  protected void assertVerkiezing(final Verkiezing verkiezing, final String id, final String naam, final String categorie) {
    assertEquals(id, verkiezing.verkiezingId(), "Niet verwachte verkiezing id");
    assertEquals(naam, verkiezing.naam(), "Niet verwachte verkiezing naam");
    assertEquals(categorie, verkiezing.categorie(), "Niet verwachte verkiezing categorie");
  }
}
