/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.ivu.elect.nl.eml.readable.ListSubmissionMode;
import de.ivu.wahl.wus.oasis.eml.EML;
import nl.h72.eml.records.Eml230b;
import nl.h72.eml.records.Kandidaat;
import nl.h72.eml.records.Kieskring;
import nl.h72.eml.records.Partij;

/**
 * Test class for {@link Eml230bOmzetter}.
 */
class Eml230bOmzetterTest extends AbstractEmlOmzetter {

  private Eml230b eml230;

  @BeforeEach
  void voorElke() throws IOException {
    final EML eml = lees("Kandidatenlijsten_PS2023_Fryslan.eml.xml");
    eml230 = new Eml230bOmzetter().zetOm(eml);
  }

  @Test
  void testVormEmlOmNaarEml230Record() {
    assertEquals("230b", eml230.emlId(), "Invoer is niet herkent als 230 bestand");
    assertVerkiezing(eml230.verkiezing(), "PS2023_Fryslan", "Provinciale Staten Fryslân 2023", "PS");
    final Kieskring kieskring = eml230.kieskringen().get(0);
    assertEquals("geen", kieskring.kieskringId(), "Kieskring id moet 'geen'");
    assertEquals(17, kieskring.partijen().size(), "Aantal partijen klopt niet");
    final Partij partij = kieskring.partijen().get(5);
    assertEquals(6, partij.partijId(), "Partij id klopt niet");
    assertEquals("GROENLINKS", partij.naam(), "Partijnaam klopt niet");
    assertSame(ListSubmissionMode.SINGLELIST, partij.lijstType(), "Partij lijst type klopt niet");
    final List<Kandidaat> kandidaten = partij.kandidaten();
    assertEquals(50, kandidaten.size(), "Komt niet overeen met verwachte aantal kandidaten");
    final Kandidaat kandidaat = kandidaten.get(4);
    assertEquals(5, kandidaat.kandidaatId(), "Kandidaat id klopt niet");
    assertEquals("vrouw", kandidaat.geslacht(), "Geslacht van kandidaat klopt niet");
    assertEquals("A.", kandidaat.voorletters(), "Voorleters van kandidaat kloppen niet");
    assertEquals("Ajla", kandidaat.roepnaam(), "Roepnaam van kandidaat klopt niet");
    assertEquals("", kandidaat.tussenvoegsel(), "Tussenvoegsel van kandidaat klopt niet");
    assertEquals("Zildžović-Jegić", kandidaat.achternaam(), "Achternaam van kandidaat klopt niet");
    assertEquals("Leeuwarden", kandidaat.woonplaats(), "Woonplaats van kandidaat klopt niet");
    assertNull(kandidaat.landCode(), "LandCode van kandidaat zou null moeten zijn, omdat niet gezet");
  }

  @Test
  void testUitgebreidAddressEml230Record() {
    final Kandidaat kandidaat = eml230.kieskringen().get(0).partijen().get(0).kandidaten().get(0);

    assertEquals("Leeuwarden", kandidaat.woonplaats(), "Woonplaats van kandidaat klopt niet");
    assertEquals("NL", kandidaat.landCode(), "LandCode van kandidaat klopt niet");
  }

}
