/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import nl.h72.eml.omzetten.EmlHulpmiddel.Stembureau;

/**
 * Test class for {@link EmlHulpmiddel}.
 */
class EmlHulpmiddelTest {

  @Test
  void testSplitStembureauNaam() {
    final String stembureau = "Stembureau Huis van Cultuur en Bestuur (postcode: 9603 AE)";
    final Stembureau record = EmlHulpmiddel.splitStembureauNaam(stembureau);

    assertEquals("Huis van Cultuur en Bestuur", record.naam(), "Naam van stembureau niet gevonden");
    assertEquals("9603AE", record.postcode(), "Postcode van stembureau niet gevonden");
  }

  @Test
  void testSplitDubbbeleStembureauNaam() {
    final String stembureau = "Stembureau Stembureau Verpleeghuis Willibrord (postcode: 4331 AC)";
    final Stembureau record = EmlHulpmiddel.splitStembureauNaam(stembureau);

    assertEquals("Verpleeghuis Willibrord", record.naam(), "Naam van stembureau niet gevonden");
    assertEquals("4331AC", record.postcode(), "Postcode van stembureau niet gevonden");
  }

  @Test
  void testVerwijderStembureauNaam() {
    final String stembureauText = "Stembureau Huis van Cultuur en Bestuur";
    final Stembureau stembureau = EmlHulpmiddel.splitStembureauNaam(stembureauText);

    assertEquals("Huis van Cultuur en Bestuur", stembureau.naam(), "Naam van stembureau niet gevonden");
  }

  @Test
  void testSplitStembureauNaamGeenMatch() {
    final String stembureauText = "Huis van Cultuur en Bestuur";
    final Stembureau stembureau = EmlHulpmiddel.splitStembureauNaam(stembureauText);

    assertEquals(stembureauText, stembureau.naam(), "Naam moet zelfde als invoer zijn");
    assertEquals("", stembureau.postcode(), "Postcode moet leeg zijn");
  }
}
