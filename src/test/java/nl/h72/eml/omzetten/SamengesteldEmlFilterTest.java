/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import nl.h72.eml.records.Filter;
import nl.h72.eml.records.GekozenKandidaat;
import nl.h72.eml.records.GekozenPartij;
import nl.h72.eml.records.Kandidaat;
import nl.h72.eml.records.KandidaatStemmen;
import nl.h72.eml.records.Kieskring;
import nl.h72.eml.records.Partij;
import nl.h72.eml.records.PartijStemmen;
import nl.h72.eml.records.RapportageGebied;
import nl.h72.eml.records.Regio;
import nl.h72.eml.records.StemStatistieken;
import nl.h72.eml.records.Verkiezing;

/**
 * Test class voor {@link SamengesteldEmlFilter}.
 */
class SamengesteldEmlFilterTest {

  private final GekozenKandidaat gekozenKandidaat = new GekozenKandidaat(0, null, null, null, null, null, null, null, null, false, 0);
  private final GekozenPartij gekozenPartij = new GekozenPartij(null, 0, null, null);
  private final Kandidaat kandidaat = new Kandidaat(0, null, null, null, null, null, null, null);
  private final KandidaatStemmen kandidaatStemmen = new KandidaatStemmen(0, null, 0);
  private final Kieskring kieskring = new Kieskring(null, null, null);
  private final Partij partij = new Partij(0, null, null, null);
  private final PartijStemmen partijStemmen = new PartijStemmen(0, null, 0, null);
  private final RapportageGebied rapportageGebied = new RapportageGebied(null, null, null, null, null, null);
  private final Regio regio = new Regio(0, null, null, null, 0, null, false, false, null);
  private final StemStatistieken stemStatistieken = new StemStatistieken(null);
  private final Verkiezing verkiezing = new Verkiezing(null, null, null, null, null, null);

  @ParameterizedTest
  @MethodSource("samengesteldEmlFilterTests")
  void testSamengesteldEmlFilter(final boolean eerste, final boolean tweede, final boolean resultaat) {
    final SamengesteldEmlFilter sFilter = new SamengesteldEmlFilter();

    sFilter.add(new TestFilter(eerste));
    sFilter.add(new TestFilter(tweede));
    assertResultaat(sFilter, resultaat, gekozenKandidaat);
    assertResultaat(sFilter, resultaat, gekozenPartij);
    assertResultaat(sFilter, resultaat, kandidaat);
    assertResultaat(sFilter, resultaat, kandidaatStemmen);
    assertResultaat(sFilter, resultaat, kieskring);
    assertResultaat(sFilter, resultaat, partij);
    assertResultaat(sFilter, resultaat, partijStemmen);
    assertResultaat(sFilter, resultaat, rapportageGebied);
    assertResultaat(sFilter, resultaat, regio);
    assertResultaat(sFilter, resultaat, stemStatistieken);
    assertResultaat(sFilter, resultaat, verkiezing);
  }

  private void assertResultaat(final SamengesteldEmlFilter sFilter, final boolean resultaat, final Filter filter) {
    final boolean resultaatFilter = filter.filter(sFilter);

    if (resultaat) {
      assertTrue(resultaatFilter);
    } else {
      assertFalse(resultaatFilter);
    }
  }

  static List<Arguments> samengesteldEmlFilterTests() {
    //@formatter:off
    return List.of(
        Arguments.of(false, false, false),
        Arguments.of(true, false, false),
        Arguments.of(false, true, false),
        Arguments.of(true, true, true)
    );
    //@formatter:on
  }

  private static class TestFilter implements EmlFilter {

    private final boolean waarde;

    public TestFilter(final boolean waarde) {
      this.waarde = waarde;
    }

    @Override
    public boolean filter(final GekozenKandidaat gekozenKandidaat) {
      return waarde;
    }

    @Override
    public boolean filter(final GekozenPartij gekozenPartij) {
      return waarde;
    }

    @Override
    public boolean filter(final Kandidaat kandidaat) {
      return waarde;
    }

    @Override
    public boolean filter(final KandidaatStemmen kandidaatStemmen) {
      return waarde;
    }

    @Override
    public boolean filter(final Kieskring kieskring) {
      return waarde;
    }

    @Override
    public boolean filter(final Partij partij) {
      return waarde;
    }

    @Override
    public boolean filter(final PartijStemmen partijStemmen) {
      return waarde;
    }

    @Override
    public boolean filter(final RapportageGebied rapportageGebied) {
      return waarde;
    }

    @Override
    public boolean filter(final Regio regio) {
      return waarde;
    }

    @Override
    public boolean filter(final StemStatistieken stemStatistieken) {
      return waarde;
    }

    @Override
    public boolean filter(final Verkiezing verkiezing) {
      return waarde;
    }
  }
}
