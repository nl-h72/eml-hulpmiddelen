/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.h72.eml.records.Eml110a;
import nl.h72.eml.records.Eml230b;
import nl.h72.eml.records.Eml510;
import nl.h72.eml.records.Eml520;
import nl.h72.eml.records.KandidaatStemmen;
import nl.h72.eml.records.LogItem;
import nl.h72.eml.records.PartijStemmen;
import nl.h72.eml.records.Regio;

/**
 * Test class for {@link EmlLezer}.
 */
@ExtendWith(MockitoExtension.class)
class EmlLezerTest {

  private @Mock EmlVerwerker verwerker;
  private @Mock Consumer<LogItem> logger;
  private @Mock EmlFilter emlFilter;
  private @Captor ArgumentCaptor<LogItem> logCaptor;

  @Test
  void testEmlLezer() throws Exception {
    final EmlLezer lezer = EmlLezer.bouwer(verwerker).logger(logger).filter(emlFilter).bouw();

    lezer.verwerk(Path.of(getClass().getResource(".").toURI()));
    verify(verwerker).verwerk(any(File.class), any(Eml110a.class));
    verify(verwerker).verwerk(any(File.class), any(Eml230b.class));
    verify(verwerker, times(3)).verwerk(any(File.class), any(Eml510.class));
    verify(verwerker).verwerk(any(File.class), any(Eml520.class));

    verify(logger, times(6)).accept(any(LogItem.class));

    verify(emlFilter, times(20)).filter(any(Regio.class));
    verify(emlFilter, times(26294)).filter(any(KandidaatStemmen.class));
    verify(emlFilter, times(794)).filter(any(PartijStemmen.class));
  }

  @Test
  void testEmlLezerLeesFout() throws Exception {
    doThrow(RuntimeException.class).when(verwerker).verwerk(any(File.class), isA(Eml110a.class));
    doThrow(RuntimeException.class).when(verwerker).verwerk(any(File.class), isA(Eml230b.class));
    doThrow(RuntimeException.class).when(verwerker).verwerk(any(File.class), isA(Eml510.class));
    doThrow(RuntimeException.class).when(verwerker).verwerk(any(File.class), isA(Eml520.class));
    final EmlLezer lezer = EmlLezer.bouwer(verwerker).logger(logger).bouw();

    lezer.verwerk(Path.of(getClass().getResource(".").toURI()));
    verify(logger, times(6)).accept(logCaptor.capture());
    final Optional<LogItem> logItem = logCaptor.getAllValues().stream().filter(li -> !li.succes()).findFirst();
    assertTrue(logItem.isPresent(), "Moet een onsuccesvol log item bevatten");
    assertNotNull(logItem.get().exception(), "Moet een exceptie bevatten");
  }

  @Test()
  void testEmlLezerIOFout() {
    final EmlLezer lezer = EmlLezer.bouwer(verwerker).logger(logger).filter(emlFilter).bouw();
    final Path pad = Path.of("123");

    assertThrows(UncheckedIOException.class, () -> lezer.verwerk(pad), "Verwacht een exceptie voor een wanneer een niet bestaande map wordt gegeven");
  }
}
