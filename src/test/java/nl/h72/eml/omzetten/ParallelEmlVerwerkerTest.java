/*
 * Copyright 2023-2024 EML-Hulpmiddelen bijdragers
 *
 * In licentie gegeven krachtens de EUPL, Versie 1.2 of – zodra ze zullen worden
 * goedgekeurd door de Europese  Commissie - latere versies van de EUPL (De "Licentie");
 *
 * U mag dit werk alleen gebruiken in overeenstemming met de licentie.
 * U kunt een kopie van de licentie verkrijgen op:
 *
 * https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 *
 * Tenzij vereist door de toepasselijke wetgeving of schriftelijk overeengekomen,
 * wordt software die onder de Licentie wordt gedistribueerd, gedistribueerd op
 * een "AS IS"-basis,  ZONDER GARANTIES OF VOORWAARDEN VAN WELKE AARD DAN OOK,
 * expliciet of impliciet.
 * Zie de Licentie voor de specifieke taal waarin de machtigingen en beperkingen
 * onder de Licentie van toepassing zijn.
 *
 * Deze licentie is niet van toepassing op de broncode in de namespaces beginnende
 * met `de.ivu`, `oasis` en `org.w3`.
 * Op die broncode is de copyright van de Kiesraad van toepassing.
 */
package nl.h72.eml.omzetten;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nl.h72.eml.omzetten.ParallelEmlVerwerker.ParallelleEmlLezer;
import nl.h72.eml.records.LogItem;

/**
 * Test class for {@link ParallelEmlVerwerker}.
 */
@ExtendWith(MockitoExtension.class)
class ParallelEmlVerwerkerTest {

  private @Mock EmlVerwerker emlVerwerker;
  private @Mock Consumer<LogItem> logger;

  @Test
  @Timeout(value = 20, unit = TimeUnit.SECONDS)
  void testVerwerk() {
    final ParallelEmlVerwerker verwerker = new ParallelEmlVerwerker(emlVerwerker, 1);
    final ParallelleEmlLezer paralleleEmlLezer = bouwer -> {
      final EmlLezer emlLezer = bouwer.logger(logger).bouw();
      try {
        emlLezer.verwerk(Path.of(getClass().getResource(".").toURI()));
      } catch (final URISyntaxException e) {
        throw new RuntimeException(e);
      }
    };
    verwerker.verwerk(paralleleEmlLezer);
    verify(logger, times(6)).accept(any(LogItem.class));
  }
}
